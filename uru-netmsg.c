/*
 * uru-netmsg.c
 * Routines for Uru dissection
 *
 * Copyright (C) 2005-2010  The Alcugs H'uru Server Team
 * See the file AUTHORS for more info about the team
 *
 * $Id$
 *
 * Wireshark - Network traffic analyzer
 * By Gerald Combs <gerald@wireshark.org>
 * Copyright 1998 Gerald Combs
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "packet-uru.h"
#include <stdio.h>

// import from packet-uru.c
extern gint ett_uru_netmsg_subtree;
extern int hf_uru_netmsg;
extern packet_info *uru_global_pinfo;


static const value_string plDestinations[] = {
	{ KAgent, "Agent" },
	{ KLobby, "Lobby" },
	{ KGame, "Game" },
	{ KVault, "Vault" },
	{ KAuth, "Auth" },
	{ KAdmin, "Admin" },
	{ KLookup, "Lookup" },
	{ KClient, "Client" },
	{ 0, NULL }
};

static const value_string plReasonCodes[] = {
	{ RStopResponding, "StopResponding" },
	{ RActive, "Active" }, // custom
	{ RInRoute, "InRoute" },
	{ RArriving, "Arriving" },
	{ RJoining, "Joining" },
	{ RLeaving, "Leaving" },
	{ RQuitting, "Quitting" },
	{ RUnknown, "Unknown" },
	{ RKickedOff, "KickedOff" },
	{ RTimedOut, "TimedOut" },
	{ RLoggedInElsewhere, "LoggedInElsewhere" },
	{ RNotAuthenticated, "NotAuthenticated" },
	{ RUnprotectedCCR, "UnprotectedCCR" },
	{ RIllegalCCRClient, "IllegalCCRClient"},
	// custom
	{ RHackAttempt, "RHackAttempt"},
	{ RUnimplemented, "RUnimplemented"},
	{ RParseError, "RParseError"},
	{ 0, NULL }
};

static const value_string plStatusFlags[] = {
	{ 0x00, "delete" },
	{ 0x01, "invisible" },
	{ 0x02, "visible" },
	{ 0x03, "buddies only" },
	{ 0, NULL }
};

static const value_string plAuthCodes[] = {
	{ AAuthSucceeded, "AuthSucceeded" },
	{ AAuthHello, "AuthHello" },
	{ AProtocolOlder, "ProtocolOlder" },
	{ AProtocolNewer, "ProtocolNewer" },
	{ AAccountExpired, "AccountExpired" },
	{ AAccountDisabled, "AccountDisabled" },
	{ AInvalidPasswd, "InvalidPasswd" },
	{ AInvalidUser, "InvalidUser" },
	{ 0, NULL }
};

static const value_string plReleases[] = {
	{ TDbg, "Debug" },
	{ TIntRel, "InternalRelease" },
	{ TExtRel, "ExternalRelease" },
	{ 0, NULL}
};

static const value_string plCreatePlayerCodes[] = {
	{ AOK, "Ok" },
	{ ANameDoesNotHaveEnoughLetters, "NameDoesNotHaveEnoughLetters" },
	{ ANameIsTooShort, "NameIsTooShort" },
	{ ANameIsTooLong, "NameIsTooLong" },
	{ AInvitationNotFound, "InvitationNotFound" },
	{ ANameIsAlreadyInUse, "NameIsAlreadyInUse" },
	{ ANameIsNotAllowed, "NameIsNotAllowed" },
	{ AMaxNumberPerAccountReached, "MaxNumberPerAccountReached" },
	{ AUnspecifiedServerError, "UnspecifiedServerError" },
	{ 0, NULL}
};

static const value_string plNetLinkingRules[] = {
	{ KBasicLink, "KBasicLink"},
	{ KOriginalBook, "KOriginalBook"},
	{ KSubAgeBook, "KSubAgeBook"},
	{ KOwnedBook, "KOwnedBook"},
	{ KVisitBook, "KVisitBook"},
	{ KChildAgeBook, "KChildAgeBook"},
	{ 0, NULL}
};

static const value_string plPlasmaObjectTypes[] = {
	// message types
	{ plAnimCmdMsg, "plAnimCmdMsg"},
	{ plControlEventMsg, "plControlEventMsg"},
	{ plLoadCloneMsg, "plLoadCloneMsg"},
	{ plEnableMsg, "plEnableMsg"},
	{ plWarpMsg, "plWarpMsg"},
	{ plServerReplyMsg, "plServerReplyMsg"},
	{ plAvatarMsg, "plAvatarMsg"},
	{ plAvTaskMsg, "plAvTaskMsg"},
	{ plAvSeekMsg, "plAvSeekMsg"},
	{ plAvOneShotMsg, "plAvOneShotMsg"},
	{ plLinkToAgeMsg, "plLinkToAgeMsg"},
	{ plNotifyMsg, "plNotifyMsg"},
	{ plLinkEffectsTriggerMsg, "plLinkEffectsTriggerMsg"},
	{ plOneShotMsg, "plOneShotMsg"},
	{ plParticleTransferMsg, "plParticleTransferMsg"},
	{ plParticleKillMsg, "plParticleKillMsg" },
	{ plAvatarInputStateMsg, "plAvatarInputStateMsg"},
	{ plLinkingMgrMsg, "plLinkingMgrMsg"},
	{ plClothingMsg, "plClothingMsg"},
	{ plInputIfaceMgrMsg, "plInputIfaceMgrMsg"},
	{ pfKIMsg, "pfKIMsg"},
	{ plAvBrainGenericMsg, "plAvBrainGenericMsg"},
	{ plMultistageModMsg, "plMultistageModMsg"},
	{ plBulletMsg, "plBulletMsg"},
	{ plLoadAvatarMsg, "plLoadAvatarMsg"},
	{ plSubWorldMsg, "plSubWorldMsg"},
	{ plClimbMsg, "plClimbMsg"},
	{ pfMarkerMsg, "pfMarkerMsg"},
	{ plAvCoopMsg, "plAvCoopMsg"},
	{ plSetNetGroupIDMsg, "plSetNetGroupIDMsg"},
	{ plPseudoLinkEffectMsg, "plPseudoLinkEffectMsg"},
	{ pfClimbingWallMsg, "pfClimbingWallMsg"},
	// vault types
	{plAgeLinkStruct, "plAgeLinkStruct"},
	{plCreatableGenericValue, "plCreatableGenericValue"},
	{plCreatableStream, "plCreatableStream"},
	{plServerGuid, "plServerGuid"},
	{plVaultNodeRef, "plVaultNodeRef"},
	{plVaultNode, "plVaultNode"},
	// NULL type and unknown
	{ plNull, "plNull"},
	{ 0, NULL}
};

static const value_string plKIMsgTypes[] = {
	// 0x01 = private, 0x08 = Inter-Age, 0x20 = hood
	{0x00, "AgeChat"},
	{0x01, "PrivateAgeChat"},
	{0x08, "BuddyChat"},
	{0x09, "PrivateInterAgeChat"},
	{0x10, "StatusMessage"},
	{0x28, "NeighborChat"},
	{ 0, NULL}
};

static const value_string proEventDataTypes[] = {
	{ proCollisionEventData, "proCollisionEventData"},
	{ proPickedEventData, "proPickedEventData"},
//	{ proControlKeyEventData, "proControlKeyEventData"},
	{ proVariableEventData, "proVariableEventData"},
	{ proFacingEventData, "proFacingEventData"},
	{ proContainedEventData, "proContainedEventData"},
	{ proActivateEventData, "proActivateEventData"},
//	{ proCallbackEventData, "proCallbackEventData"},
	{ proResponderStateEventData, "proResponderStateEventData"},
	{ proMultiStateEventData, "proMultiStateEventData"},
	{ proSpawnedEventData, "proSpawnedEventData"},
//	{ proClickDragEventData, "proClickDragEventData"},
//	{ proCoopEventData, "proCoopEventData"},
	{ proOfferLinkBookEventData, "proOfferLinkBookEventData"},
//	{ proBookEventData, "proBookEventData"},
//	{ proClimbingBlockHitEventData, "proClimbingBlockHitEventData"},
	{ 0, NULL}
};

static const value_string plVaultZlibs[] = {
	{ 0x01, "uncompressed" },
	{ 0x03, "compressed" },
	{ 0, NULL}
};

static const value_string plVaultTasks[] = {
	{0x01, "TCreatePlayer"},
	{0x02, "TDeletePlayer"},
	{0x03, "TGetPlayerList"},
	{0x04, "TCreateNeighborhood"},
	{0x05, "TJoinNeighborhood"},
	{0x06, "TSetAgePublic"},
	{0x07, "TIncPlayerOnlineTime"},
	{0x08, "TEnablePlayer"},
	{0x09, "TRegisterOwnedAge"},
	{0x0A, "TUnRegisterOwnedAge"},
	{0x0B, "TRegisterVisitAge"},
	{0x0C, "TUnRegisterVisitAge"},
	{0x0D, "TFriendInvite"},
	{ 0, NULL}
};

static const value_string plVaultTaskIDs[] = {
	{1, "ID of created age link"},
	{11, "age link"},
	{12, "filename of age to remove"},
	{13, "GUID of invite to remove"},
	{ 0, NULL}
};

static const value_string plVaultCmds[] = {
	{0x01, "VConnect"},
	{0x02, "VDisconnect"},
	{0x03, "VAddNodeRef"},
	{0x04, "VRemoveNodeRef"},
	{0x05, "VNegotiateManifest"},
	{0x06, "VSaveNode"},
	{0x07, "VFindNode"},
	{0x08, "VFetchNode"},
	{0x09, "VSendNode"},
	{0x0A, "VSetSeen"},
	{0x0B, "VOnlineState"},
	{ 0, NULL}
};

static const value_string plVaultCmdIDs[] = {
	{0, "Unknown ID sent by VaultManager (must be 0xC0AB3041)"},
	{1, "Node type"},
	{2, "KI number"},
	{4, "Reviever of sent node"},
	{5, "vault node"},
	{6, "Stream of vault nodes"},
	{7, "Vault node ref"},
	{9, "Node ID / Son of a ref"},
	{10, "Stream containing a list of IDs"},
	{11, "New node ID (VSaveNode)"},
	{13, "Parent of a ref"},
	{14, "Stream of manifests"},
	{15, "Stream of refs"},
	{16, "Unknown (must be 0 or 1, seen in VFindNode)"},
	{19, "Seen flag"},
	{20, "Unknown ID of -1"},
	{21, "Age name"},
	{22, "Age GUID"},
	{23, "Name of vault"},
	{24, "Timestamp of found node"},
	{25, "Number of vault nodes"},
	{27, "Current age of a player"},
	{28, "Current age GUID of a player"},
	{29, "Online status of a player"},
	{31, "End of vault node transmission"},
	{ 0, NULL}
};

static const value_string plVaultNodeTypes[] = {
	{0x00, "KInvalidNode"},
	{0x02, "KVNodeMgrPlayerNode"},
	{0x03, "KVNodeMgrAgeNode"},
	{0x04, "KVNodeMgrGameServerNode"},
	{0x05, "KVNodeMgrAdminNode"},
	{0x06, "KVNodeMgrServerNode"},
	{0x07, "KVNodeMgrCCRNode"},
	{0x16, "KFolderNode"},
	{0x17, "KPlayerInfoNode"},
	{0x18, "KSystem"},
	{0x19, "KImageNode"},
	{0x1A, "KTextNoteNode"},
	{0x1B, "KSDLNode"},
	{0x1C, "KAgeLinkNode"},
	{0x1D, "KChronicleNode"},
	{0x1E, "KPlayerInfoListNode"},
	{0x20, "KMarkerNode"},
	{0x21, "KAgeInfoNode"},
	{0x22, "KAgeInfoListNode"},
	{0x23, "KMarkerListNode;"},
	{ 0, NULL}
};

static const value_string plVaultFolderTypes[] = {
	{0, "KGeneric"},
	{1, "KInboxFolder"},
	{2, "KBuddyListFolder"},
	{3, "KIgnoreListFolder"},
	{4, "KPeopleIKnowAboutFolder"},
	{5, "KVaultMgrGlobalDataFolder"},
	{6, "KChronicleFolder"},
	{7, "KAvatarOutfitFolder"},
	{8, "KAgeTypeJournalFolder"},
	{9, "KSubAgesFolder"},
	{10, "KDeviceInboxFolder"},
	{11, "KHoodMembersFolder"},
	{12, "KAllPlayersFolder"},
	{13, "KAgeMembersFolder"},
	{14, "KAgeJournalsFolder"},
	{15, "KAgeDevicesFolder"},
	{16, "KAgeInstaceSDLNode"},
	{17, "KAgeGlobalSDLNode"},
	{18, "KCanVisitFolder"},
	{19, "KAgeOwnersFolder"},
	{20, "KAllAgeGlobalSDLNodesFolder"},
	{21, "KPlayerInfoNodeFolder"},
	{22, "KPublicAgesFolder"},
	{23, "KAgesIOwnFolder"},
	{24, "KAgesICanVisitFolder"},
	{25, "KAvatarClosetFolder"},
	{26, "KAgeInfoNodeFolder"},
	{27, "KSystemNode"},
	{28, "KPlayerInviteFolder"},
	{29, "KCCRPlayersFolder"},
	{30, "KGlobalInboxFolder"},
	{31, "KChildAgesFolder"},
	{ 0, NULL}
};

// all this is based on information from the p* files of unet3 and unet2

// very low-level helper functions
static guint16 getUStrLength(tvbuff_t *tvb, guint32 offset, guint8 *inv) {
	guint16 size=tvb_get_letohs(tvb, offset);
	*inv = (size & 0xF000) != 0;
	size &= ~0xF000; // remove highest bit
	return size;
}

// low-level helper functions
static guint32 alcDecodeUStr(gchar* out, tvbuff_t *tvb, guint32 offset, guint32 max_size, guint8 *inv) {
	guint32 i;
	// get size
	guint16 size = getUStrLength(tvb, offset, inv);
	offset += 2;
	if(size>max_size) size=max_size;
	
	// get content
	for(i=0; i<size; i++) {
		if (*inv) { out[i]=~tvb_get_guint8(tvb, offset); }
		else { out[i]=tvb_get_guint8(tvb, offset); }
		++offset;
	}
	out[i]='\0'; // be sure that is correctly ended!

	return size+2; // the amount of read bytes
}
static void alcHex2Ascii(gchar * out, tvbuff_t *tvb, guint32 offset, guint32 size) {
	guint32 i; guint8 in;
	for(i=0; i<size; i++) {
		in = tvb_get_guint8(tvb, offset+i);
		out[2*i]=  ((in & 0xF0)>>4);
		out[2*i]= (out[2*i]<0x0A ? out[2*i]+0x30 : out[2*i]+(0x41-0x0A));
		out[(2*i)+1] = ((in & 0x0F)<0x0A ? (in & 0x0F)+0x30 : (in & 0x0F)+(0x41-0x0A));
	}
	out[size*2]='\0';
}

/* mid-level helper functions (using above helper functions)*/
static guint32 addUStr(tvbuff_t *tvb, guint32 offset, proto_tree *tree, char *desc)
{
	gchar str[4096];
	guint8 inv;
	guint32 size = alcDecodeUStr(str, tvb, offset, 4095, &inv);
	
	proto_tree_add_none_format(tree, hf_uru_netmsg, tvb, offset, size, "%s: %s%s", desc, str, inv ? " (inverted)" : "");
	return size;
}
static guint32 addUStr2(tvbuff_t *tvb, guint32 offset, proto_tree *tree, char *desc, char *str, guint32 max_size)
// saves the string in an external buffer
{
	guint8 inv;
	guint32 size = alcDecodeUStr(str, tvb, offset, max_size, &inv);
	
	proto_tree_add_none_format(tree, hf_uru_netmsg, tvb, offset, size, "%s: %s%s", desc, str, inv ? " (inverted)" : "");
	return size;
}
static guint32 addUStrChecked(tvbuff_t *tvb, guint32 offset, proto_tree *tree, char *desc, char *correct)
{
	gchar str[4096];
	guint8 inv;
	guint32 size = alcDecodeUStr(str, tvb, offset, 4095, &inv);
	
	proto_tree_add_none_format(tree, hf_uru_netmsg, tvb, offset, size, "%s: %s%s", desc, str, inv ? " (inverted)" : "");
	if (strcmp(str, correct) != 0)
		uru_parse_error(tree, tvb, offset, size, "Invalid value: \"%s\" != \"%s\"", str, correct);
	return size;
}
static guint32 addHexUStr(tvbuff_t *tvb, guint32 offset, proto_tree *tree, char *desc)
{
	gchar str[4096];
	guint8 inv;
	// get size
	guint16 size = getUStrLength(tvb, offset, &inv);
	// show content
	alcHex2Ascii(str, tvb, offset+2, size);
	
	proto_tree_add_none_format(tree, hf_uru_netmsg, tvb, offset, size+2, "%s: %s", desc, str);
	if (inv) uru_parse_error(tree, tvb, offset, size+2, "Hex UruStrings must not be inverted");
	offset += size;
	
	return size+2;
}
static guint32 addHex2Ascii(tvbuff_t *tvb, guint32 offset, guint32 size, proto_tree *tree, char *desc)
{
	gchar str[101];
	alcHex2Ascii(str, tvb, offset, size > 50 ? 50 : size);
	
	proto_tree_add_none_format(tree, hf_uru_netmsg, tvb, offset, size, "%s: %s", desc, str);
	return size;
}
static guint32 addHex2Ascii2(tvbuff_t *tvb, guint32 offset, guint32 size, proto_tree *tree, char *desc, char *str)
{
	alcHex2Ascii(str, tvb, offset, size);
	proto_tree_add_none_format(tree, hf_uru_netmsg, tvb, offset, size, "%s: %s", desc, str);
	return size;
}
static guint32 addIPStr(tvbuff_t *tvb, guint32 offset, proto_tree *tree, char *desc, guint8 networkByteOrder) {
	struct in_addr addr;
	if (networkByteOrder)
		addr.s_addr=tvb_get_ipv4(tvb, offset);
	else
		addr.s_addr=tvb_get_ntohl(tvb, offset);
	proto_tree_add_none_format(tree, hf_uru_netmsg, tvb, offset, 4, "%s: %s", desc, inet_ntoa(addr));
	return 4;
}

/* functions dissecting objects found in messages (high-level helper functions) */
static guint32 dissectVaultNode(proto_tree *sub_tree, tvbuff_t *rtvb, guint32 offset)
{
	gint flagSize, flagB, flagC;
	guint32 var32_1, var32_2;
	guint8 type;
	// add a subtree for the node
	guint32 startOffset = offset;
	proto_item *tf = proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 0 /* we dont know the size yet */, "Node");
	sub_tree = proto_item_add_subtree(tf, ett_uru_netmsg_subtree);
	
	// flags
	flagSize = tvb_get_letohl(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Size of bit vector: %d", flagSize);
	if (flagSize < 1 || flagSize > 2) uru_parse_error(sub_tree, rtvb, offset, 4, "Invalid size: Must be 1 or 2, not %d", flagSize);
	offset += 4;
	
	flagB = tvb_get_letohl(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Flags B: 0x%08X", flagB);
	offset += 4;
	if (flagSize == 2) {
		flagC = tvb_get_letohl(rtvb, offset);
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Flags C: 0x%08X", flagC);
		offset += 4;
	}
	else
		flagC = 0;
	
	// mandatory fields
	var32_1 = tvb_get_letohl(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Index: %d", var32_1);
	proto_item_append_text(tf, " %d", var32_1);
	offset += 4;
	
	type = tvb_get_guint8(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Type: 0x%02X (%s)", type, val_to_str(type, plVaultNodeTypes, "Unknown"));
	++offset;
	
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Permissions: 0x%08X", tvb_get_letohl(rtvb, offset));
	offset += 4;
	
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Owner: %d", tvb_get_letohl(rtvb, offset));
	offset += 4;
	
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Group: %d", tvb_get_letohl(rtvb, offset));
	offset += 4;
	
	var32_1 = tvb_get_letohl(rtvb, offset);
	var32_2 = tvb_get_letohl(rtvb, offset+4);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 8, "Modify Time: %u.%06u", var32_1, var32_2);
	offset += 8;
	
	// optional fields
	if (flagB & 0x00000040) { // creator
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Creator: %d", tvb_get_letohl(rtvb, offset));
		offset += 4;
	}
	if (flagB & 0x00000080) { // create time
		var32_1 = tvb_get_letohl(rtvb, offset);
		var32_2 = tvb_get_letohl(rtvb, offset+4);
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 8, "Create Time: %u.%06u", var32_1, var32_2);
		offset += 8;
	}
	if (flagB & 0x00000200) { // age time
		var32_1 = tvb_get_letohl(rtvb, offset);
		var32_2 = tvb_get_letohl(rtvb, offset+4);
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 8, "Age Time: %u.%06u", var32_1, var32_2);
		offset += 8;
	}
	if (flagB & 0x00000400) // age name
		offset += addUStr(rtvb, offset, sub_tree, "Age Name");
	if (flagB & 0x00000800) // age GUID
		offset += addHex2Ascii(rtvb, offset, 8, sub_tree, "Age GUID");
	if (flagB & 0x00001000) { // Int32_1
		var32_1 = tvb_get_letohl(rtvb, offset);
		if (type == 0x16/*KFolderNode*/ || type == 0x1E/*KPlayerInfoListNode*/ || type == 0x22/*KAgeInfoListNode*/)
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Int32_1: %d (%s)", var32_1, val_to_str(var32_1, plVaultFolderTypes, "Unknown"));
		else
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Int32_1: %d", var32_1);
			
		offset += 4;
	}
	if (flagB & 0x00002000) { // Int32_2
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Int32_2: %d", tvb_get_letohl(rtvb, offset));
		offset += 4;
	}
	if (flagB & 0x00004000) { // Int32_3
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Int32_3: %d", tvb_get_letohl(rtvb, offset));
		offset += 4;
	}
	if (flagB & 0x00008000) { // Int32_4
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Int32_4: %d", tvb_get_letohl(rtvb, offset));
		offset += 4;
	}
	if (flagB & 0x00010000) { // UInt32_1
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "UInt32_1: %d", tvb_get_letohl(rtvb, offset));
		offset += 4;
	}
	if (flagB & 0x00020000) { // UInt32_2
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "UInt32_2: %d", tvb_get_letohl(rtvb, offset));
		offset += 4;
	}
	if (flagB & 0x00040000) { // UInt32_3
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "UInt32_3: %d", tvb_get_letohl(rtvb, offset));
		offset += 4;
	}
	if (flagB & 0x00080000) { // UInt32_4
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "UInt32_4: %d", tvb_get_letohl(rtvb, offset));
		offset += 4;
	}
	if (flagB & 0x00100000) // String64_1
		offset += addUStr(rtvb, offset, sub_tree, "String64_1");
	if (flagB & 0x00200000) // String64_2
		offset += addUStr(rtvb, offset, sub_tree, "String64_2");
	if (flagB & 0x00400000) // String64_3
		offset += addUStr(rtvb, offset, sub_tree, "String64_3");
	if (flagB & 0x00800000) // String64_4
		offset += addUStr(rtvb, offset, sub_tree, "String64_4");
	if (flagB & 0x01000000) // String64_5
		offset += addUStr(rtvb, offset, sub_tree, "String64_5");
	if (flagB & 0x02000000) // String64_6
		offset += addUStr(rtvb, offset, sub_tree, "String64_6");
	if (flagB & 0x04000000) // lString64_1
		offset += addUStr(rtvb, offset, sub_tree, "lString64_1");
	if (flagB & 0x08000000) // lString64_2
		offset += addUStr(rtvb, offset, sub_tree, "lString64_2");
	if (flagB & 0x10000000) // Text_1
		offset += addUStr(rtvb, offset, sub_tree, "Text_1");
	if (flagB & 0x20000000) // Text_2
		offset += addUStr(rtvb, offset, sub_tree, "Text_2");
	if (flagB & 0x40000000) { // Blob1
		var32_1 = tvb_get_letohl(rtvb, offset);
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Blob1 Size: %d", var32_1);
		offset += 4;
		if (var32_1 > 0) {
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, var32_1, "Blob1");
			offset += var32_1;
		}
	}
	if (flagB & 0x80000000) { // Blob2
		var32_1 = tvb_get_letohl(rtvb, offset);
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Blob2 Size: %d", var32_1);
		offset += 4;
		if (var32_1 > 0) {
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, var32_1, "Blob2");
			offset += var32_1;
		}
	}
	if (flagC & 0x00000001) // Blob1 GUID
		offset += addHex2Ascii(rtvb, offset, 8, sub_tree, "Blob1 GUID (must be all zeros)");
	if (flagC & 0x00000002) // Blob2 GUID
		offset += addHex2Ascii(rtvb, offset, 8, sub_tree, "Blob2 GUID (must be all zeros)");
	
	proto_item_set_len(tf, offset-startOffset); // set the correct size
	return offset;
}

static guint32 dissectVaultNodeRef(proto_tree *sub_tree, tvbuff_t *rtvb, guint32 offset)
{
	guint32 var32_1, var32_2;
	// add a subtree for the ref
	guint32 startOffset = offset;
	proto_item *tf = proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 0 /* we dont know the size yet */, "Node Ref");
	sub_tree = proto_item_add_subtree(tf, ett_uru_netmsg_subtree);
	
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Saver: %d", tvb_get_letohl(rtvb, offset));
	offset += 4;
	
	var32_1 = tvb_get_letohl(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Parent: %d", var32_1);
	proto_item_append_text(tf, " %d", var32_1);
	offset += 4;
	
	var32_1 = tvb_get_letohl(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Child: %d", var32_1);
	proto_item_append_text(tf, " -> %d", var32_1);
	offset += 4;
	
	var32_1 = tvb_get_letohl(rtvb, offset);
	var32_2 = tvb_get_letohl(rtvb, offset+4);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 8, "Time: %u.%06u", var32_1, var32_2);
	offset += 8;
	
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Seen: 0x%02X", tvb_get_guint8(rtvb, offset));
	++offset;
	
	proto_item_set_len(tf, offset-startOffset); // set the correct size
	return offset;
}

static guint32 dissectAgeInfoStruct(proto_tree *sub_tree, tvbuff_t *rtvb, guint32 offset)
{
	char str[256];
	guint32 startOffset = offset;
	proto_item *tf = proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 0 /* we dont know the size yet */, "Age Info");
	proto_tree *info_tree = proto_item_add_subtree(tf, ett_uru_netmsg_subtree);
	
	guint8 flags = tvb_get_guint8(rtvb, offset);
	proto_tree_add_none_format(info_tree, hf_uru_netmsg, rtvb, offset, 1, "Flags: 0x%02X", flags);
	++offset;
	
	// filename
	offset += addUStr2(rtvb, offset, info_tree, "Filename", str, 255);
	proto_item_append_text(tf, " (%s)", str);
	
	if (flags & 0x01) // instance name
		offset += addUStr(rtvb, offset, info_tree, "Instance Name");
	if (flags & 0x04) // GUID
		offset += addHex2Ascii(rtvb, offset, 8, info_tree, "GUID");
	if (flags & 0x08) // user defined name
		offset += addUStr(rtvb, offset, info_tree, "User Defined Name");
	if (flags & 0x10) { // sequence number
		proto_tree_add_none_format(info_tree, hf_uru_netmsg, rtvb, offset, 4, "Sequence number: %d", tvb_get_letohl(rtvb, offset));
		offset += 4;
	}
	if (flags & 0x20) // display name
		offset += addUStr(rtvb, offset, info_tree, "Display Name");
	if (flags & 0x40) { // language
		guint32 var32 = tvb_get_letohl(rtvb, offset);
		proto_tree_add_none_format(info_tree, hf_uru_netmsg, rtvb, offset, 4, "Language: %d", var32);
		if (var32 != 0) uru_parse_error(info_tree, rtvb, offset, 4, "Invalid value: %d != 0", var32);
		offset += 4;
	}
	
	proto_item_set_len(tf, offset-startOffset); // set the correct size
	return offset;
}

static guint32 dissectAgeLinkStruct(proto_tree *sub_tree, tvbuff_t *rtvb, guint32 offset)
{
	gchar str[256];
	guint32 var32;
	guint16 flags = tvb_get_letohs(rtvb, offset);
	guint8 var8;
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Flags: 0x%04X", flags);
	offset += 2;
	
	offset = dissectAgeInfoStruct(sub_tree, rtvb, offset);
	
	var8 = tvb_get_guint8(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Linking Rule: 0x%02X (%s)", var8, val_to_str(var8, plNetLinkingRules, "Unknown"));
	++offset;
	
	var32 = tvb_get_letohl(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Unknown: %d", var32);
	if (var32 != 1) uru_parse_error(sub_tree, rtvb, offset, 4, "Invalid value: %d != 1", var32);
	offset += 4;
	
	// the spawn point
	{
		guint32 startOffset = offset;
		proto_item *tf = proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 0 /* we dont know the size yet */, "Spawn Point");
		proto_tree *spoint_tree = proto_item_add_subtree(tf, ett_uru_netmsg_subtree);
		
		guint32 flags = tvb_get_letohl(rtvb, offset);
		proto_tree_add_none_format(spoint_tree, hf_uru_netmsg, rtvb, offset, 4, "Flags: 0x%08X", flags);
		if (flags != 0x00000007) uru_parse_error(spoint_tree, rtvb, offset, 4, "Invalid value: 0x%08X != 0x00000007", flags);
		offset += 4;
		
		offset += addUStr(rtvb, offset, spoint_tree, "Title");
		
		// name
		offset += addUStr2(rtvb, offset, spoint_tree, "Name", str, 255);
		proto_item_append_text(tf, " (%s)", str);
		
		offset += addUStr(rtvb, offset, spoint_tree, "Camera Stack");
		
		proto_item_set_len(tf, offset-startOffset); // set the correct size
	}
	// end of spawn point
	
	if (flags & 0x0010) { // CCR
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "CCR: 0x%02X", tvb_get_guint8(rtvb, offset));
		++offset;
	}
	if (flags & 0x0040) // Parent age name
		offset += addUStr(rtvb, offset, sub_tree, "Parent age name");
	
	return offset;
}

static guint32 dissectVaultItem(proto_tree *sub_tree, guint16 netmsgtype, int i, tvbuff_t *rtvb, guint32 offset)
{
	gchar str[256];
	guint32 endOffset;
	guint32 size;
	guint16 type, realType;
	// add a subtree for the item
	guint32 startOffset = offset;
	proto_item *item = proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 0 /* we dont know the size yet */, "Item %d", i);
	proto_tree *item_tree = proto_item_add_subtree(item, ett_uru_netmsg_subtree);
	proto_item *tf;
	
	guint8 id = tvb_get_guint8(rtvb, offset), var8;
	if (netmsgtype == NetMsgVaultTask) {
		proto_tree_add_none_format(item_tree, hf_uru_netmsg, rtvb, offset, 1, "ID: 0x%02X (%d, %s)", id, id, val_to_str(id, plVaultTaskIDs, "Unknown"));
		proto_item_append_text(item, " (%s)", val_to_str(id, plVaultTaskIDs, "Unknown"));
	}
	else {
		proto_tree_add_none_format(item_tree, hf_uru_netmsg, rtvb, offset, 1, "ID: 0x%02X (%d, %s)", id, id, val_to_str(id, plVaultCmdIDs, "Unknown"));
		proto_item_append_text(item, " (%s)", val_to_str(id, plVaultCmdIDs, "Unknown"));
	}
	++offset;
	
	var8 = tvb_get_guint8(rtvb, offset);
	proto_tree_add_none_format(item_tree, hf_uru_netmsg, rtvb, offset, 1, "Unknown: 0x%02X", var8);
	if (var8 != 0x00) uru_parse_error(item_tree, rtvb, offset, 1, "Invalid value: 0x%02X != 0x00", var8);
	++offset;
	
	type = tvb_get_letohs(rtvb, offset);
	realType = type;
	if (netmsgtype == NetMsgVault-1) { // UU uses different type IDs
		if (type == plVaultNode-1) realType = plVaultNode;
		else if (type == plVaultNodeRef-1) realType = plVaultNodeRef;
	}
	proto_tree_add_none_format(item_tree, hf_uru_netmsg, rtvb, offset, 2, "Type: 0x%04X (%s)", type, val_to_str(realType, plPlasmaObjectTypes, "Unknown"));
	offset += 2;
	
	switch (realType) {
		case plAgeLinkStruct:
			offset = dissectAgeLinkStruct(item_tree, rtvb, offset);
			break;
		case plCreatableGenericValue:
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(item_tree, hf_uru_netmsg, rtvb, offset, 1, "Subtype: 0x%02X", var8);
			++offset;
			if (var8 == DInteger) {
				guint32 val = tvb_get_letohl(rtvb, offset);
				proto_tree_add_none_format(item_tree, hf_uru_netmsg, rtvb, offset, 4, "Integer value: 0x%08X (%d)", val, val);
				proto_item_append_text(item, " = %d", val);
				offset += 4;
			}
			else if (var8 == DUruString) {
				offset += addUStr2(rtvb, offset, item_tree, "String value", str, 255);
				proto_item_append_text(item, " (%s)", str);
			}
			else if (var8 == DTimestamp) {
				double time = tvb_get_letohieee_double(rtvb, offset);
				proto_tree_add_none_format(item_tree, hf_uru_netmsg, rtvb, offset, 8, "Time value: %f", time);
				proto_item_append_text(item, " = %f", time);
				offset += 8;
			}
			break;
		case plCreatableStream:
			size = tvb_get_letohl(rtvb, offset);
			proto_tree_add_none_format(item_tree, hf_uru_netmsg, rtvb, offset, 4, "Size: %d", size);
			proto_item_append_text(item, " (Size: %d)", size);
			offset += 4;
			
			tf = proto_tree_add_none_format(item_tree, hf_uru_netmsg, rtvb, offset, size, "Stream content");
			endOffset = offset+size;
			item_tree = proto_item_add_subtree(tf, ett_uru_netmsg_subtree);
			if (id == 6) {
				gint num = 0;
				while (offset < endOffset) {
					offset = dissectVaultNode(item_tree, rtvb, offset);
					++num;
				}
				proto_item_append_text(tf, " (%d nodes)", num);
			}
			else if (id == 10) {
				guint16 num = tvb_get_letohs(rtvb, offset), i;
				proto_tree_add_none_format(item_tree, hf_uru_netmsg, rtvb, offset, 2, "Number of IDs: %d", num);
				proto_item_append_text(tf, " (%d IDs)", num);
				offset += 2;
				for (i = 0; i < num; ++i) {
					proto_tree_add_none_format(item_tree, hf_uru_netmsg, rtvb, offset, 4, "Node ID: %d", tvb_get_letohl(rtvb, offset));
					offset += 4;
				}
			}
			else if (id == 14) {
				guint32 num = tvb_get_letohl(rtvb, offset), i;
				proto_tree_add_none_format(item_tree, hf_uru_netmsg, rtvb, offset, 4, "Number of manifests: %d", num);
				proto_item_append_text(tf, " (%d manifests)", num);
				offset += 4;
				for (i = 0; i < num; ++i) {
					// add a subtree for the ref
					gint startOffset = offset;
					proto_item *tf = proto_tree_add_none_format(item_tree, hf_uru_netmsg, rtvb, offset, 0 /* we dont know the size yet */, "Node Manifest");
					proto_tree *manifest_tree = proto_item_add_subtree(tf, ett_uru_netmsg_subtree);
					
					guint32 index = tvb_get_letohl(rtvb, offset);
					proto_tree_add_none_format(manifest_tree, hf_uru_netmsg, rtvb, offset, 4, "Node ID: %d", index);
					proto_item_append_text(tf, " for ID %d", index);
					offset += 4;
					
					proto_tree_add_none_format(manifest_tree, hf_uru_netmsg, rtvb, offset, 8, "Timestamp: %f", tvb_get_letohieee_double(rtvb, offset));
					offset += 8;
					
					proto_item_set_len(tf, offset-startOffset); // set the correct size
				}
			}
			else if (id == 15) {
				guint32 num = tvb_get_letohl(rtvb, offset), i;
				proto_tree_add_none_format(item_tree, hf_uru_netmsg, rtvb, offset, 4, "Number of refs: %d", num);
				proto_item_append_text(tf, " (%d refs)", num);
				offset += 4;
				for (i = 0; i < num; ++i)
					offset = dissectVaultNodeRef(item_tree, rtvb, offset);
			}
			else
				offset += size;
			break;
		case plServerGuid:
			offset += addHex2Ascii2(rtvb, offset, 8, item_tree, "Server GUID", str);
			proto_item_append_text(item, " = %s", str);
			break;
		case plVaultNodeRef:
			offset = dissectVaultNodeRef(item_tree, rtvb, offset);
			break;
		case plVaultNode:
			offset = dissectVaultNode(item_tree, rtvb, offset);
			break;
	}
	
	
	proto_item_set_len(item, offset-startOffset); // set the correct size
	return offset;
}

static guint32 dissectUruObject(proto_tree *sub_tree, tvbuff_t *rtvb, guint32 offset, gchar *name, guint8 withFlag)
{
	guint8 flags, hasObj = 1;
	gchar str[256];
	// add a subtree
	guint32 startOffset = offset;
	proto_item *tf = proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 0 /* we dont know the size yet */, "%s", name);
	
	if (withFlag) {
		hasObj = tvb_get_guint8(rtvb, offset);
		++offset;
		if (hasObj) proto_item_append_text(tf, " is present");
		else proto_item_append_text(tf, " is not present");
	}

	if (hasObj) {
		sub_tree = proto_item_add_subtree(tf, ett_uru_netmsg_subtree);
		
		flags = tvb_get_guint8(rtvb, offset);
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Flags: 0x%02X", flags);
		++offset;
		
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Page ID: 0x%08X", tvb_get_letohl(rtvb, offset));
		offset += 4;
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Page Type: 0x%04X", tvb_get_letohs(rtvb, offset));
		offset += 2;
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Type: 0x%04X", tvb_get_letohs(rtvb, offset));
		offset += 2;
		
		offset += addUStr2(rtvb, offset, sub_tree, "Name", str, 255);
		proto_item_append_text(tf, " (Name: %s)", str);
		
		if (flags == 0x01) { // the client ID is included
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Clone ID: %d", tvb_get_letohl(rtvb, offset));
			offset += 4;
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Clone Player ID: %d", tvb_get_letohl(rtvb, offset));
			offset += 4;
		}
	}
	
	proto_item_set_len(tf, offset-startOffset); // set the correct size
	return offset;
}

static guint32 dissectStreamedObject(proto_tree *sub_tree, tvbuff_t *rtvb, guint32 offset, tvbuff_t **sub_tvb, guint16 *type)
{
	guint8 compression;
	guint32 sentSize, realSize;

	realSize = tvb_get_letohl(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Real Size: %d", realSize);
	offset += 4;
	
	compression = tvb_get_guint8(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Compression: 0x%02X", compression);
	if (compression != 0 && compression != 2 && compression != 3)
		uru_parse_error(sub_tree, rtvb, offset, 1, "Compression must be 0x00, 0x02 or 0x03");
	else if (compression == 0x02 && !type)
		uru_parse_error(sub_tree, rtvb, offset, 1, "Untyped objects must not be compressed");
	else if (compression != 2 && realSize != 0)
		uru_parse_error(sub_tree, rtvb, offset-4, 4, "Real Size must only be set for compressed objects");
	++offset;
	
	sentSize = tvb_get_letohl(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Sent Size: %d", sentSize);
	offset += 4;
	
	if (sentSize == 0) {
		// noting was sent
		*sub_tvb = NULL;
		return offset;
	}
	
	realSize -= 2; // strip out two bytes (the object type)
	sentSize -= 2; // strip out two bytes (the object type)
	
	if (type) {
		*type = tvb_get_letohs(rtvb, offset);
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Object Type: 0x%04X (%s)", *type, val_to_str(*type, plPlasmaObjectTypes, "Unknown"));
	}
	offset += 2;
	
	// now, we may have to decompress the buffer
	if (compression == 0x02 && type) { // it's compressed
		*sub_tvb = tvb_uncompress(rtvb, offset, sentSize);
		tvb_set_child_real_data_tvbuff(rtvb, *sub_tvb);
		add_new_data_source(uru_global_pinfo, *sub_tvb, "Decompressed Streamed Object");
		tvb_set_free_cb(*sub_tvb, g_free);
		if (tvb_length(*sub_tvb) != realSize)
			uru_parse_error(sub_tree, *sub_tvb, 0, 0, "Decompressed size mismatch: %d != %d", tvb_length(*sub_tvb), realSize);
	}
	else { // it's not compressed, use a subpart of the buffer we got
		if (type)
			*sub_tvb = tvb_new_subset(rtvb, offset, sentSize, sentSize);
		else
			*sub_tvb = tvb_new_subset(rtvb, offset-2, sentSize+2, sentSize+2);
	}
	offset += sentSize; // now offset points to where we should be at the end of parsing the inner stuff
	return offset;
}

static guint32 dissectPlasmaMessage(proto_tree *sub_tree, tvbuff_t *rtvb, guint32 offset)
{
	gint nRef, i;
	guint32 var32;
	offset = dissectUruObject(sub_tree, rtvb, offset, "Sender", /*withFlag*/1);
	
	nRef = tvb_get_letohl(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Number of receivers: %d", nRef);
	offset += 4;
	for (i = 1; i <= nRef; ++i) {
		gchar name[30];
		snprintf(name, 30, "Receiver %d", i);
		offset = dissectUruObject(sub_tree, rtvb, offset, name, /*withFlag*/1);
	}
	
	var32 = tvb_get_letohl(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Unknown: %d", var32);
	if (var32 != 0) uru_parse_error(sub_tree, rtvb, offset, 4, "Invalid value: %d != 0", var32);
	offset += 4;
	
	var32 = tvb_get_letohl(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Unknown: %d", var32);
	if (var32 != 0) uru_parse_error(sub_tree, rtvb, offset, 4, "Invalid value: %d != 0", var32);
	offset += 4;
	
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Flags: 0x%08X", tvb_get_letohl(rtvb, offset));
	offset += 4;
	
	return offset;
}

static guint32 dissectProEventData(proto_tree *sub_tree, tvbuff_t *rtvb, guint32 offset, guint32 i)
{
	guint32 type, var32;
	proto_item *tf = proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 0, "Event Data %d", i);
	sub_tree = proto_item_add_subtree(tf, ett_uru_netmsg_subtree);
	
	type = tvb_get_letohs(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Type: 0x%04X (%s)", type, val_to_str(type, proEventDataTypes, "Unknown"));
	proto_item_append_text(tf, " (%s)", val_to_str(type, proEventDataTypes, "Unknown"));
	offset += 4;
	
	switch (type) {
		case proCollisionEventData:
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Is enter: 0x%02X", tvb_get_guint8(rtvb, offset));
			++offset;
			
			offset = dissectUruObject(sub_tree, rtvb, offset, "Hitter", /*withFlag*/1);
			offset = dissectUruObject(sub_tree, rtvb, offset, "Hittee", /*withFlag*/1);
			break;
		case proPickedEventData:
			offset = dissectUruObject(sub_tree, rtvb, offset, "Picker", /*withFlag*/1);
			offset = dissectUruObject(sub_tree, rtvb, offset, "Picked", /*withFlag*/1);
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Is enabled: 0x%02X", tvb_get_guint8(rtvb, offset));
			++offset;
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Hitpoint X: %f", tvb_get_letohieee_float(rtvb, offset));
			offset += 4;
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Hitpoint Y: %f", tvb_get_letohieee_float(rtvb, offset));
			offset += 4;
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Hitpoint Z: %f", tvb_get_letohieee_float(rtvb, offset));
			offset += 4;
			break;
		case proVariableEventData:
			offset += addUStr(rtvb, offset, sub_tree, "Name");
			
			var32 = tvb_get_letohl(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Data Type: %d", var32);
			if (var32 != 1) uru_parse_error(sub_tree, rtvb, offset, 4, "Invalid value: %d != 1", var32);
			offset += 4;
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Number: %f", tvb_get_letohieee_float(rtvb, offset));
			offset += 4;
			
			offset = dissectUruObject(sub_tree, rtvb, offset, "Key", /*withFlag*/1);
			break;
		case proFacingEventData:
			offset = dissectUruObject(sub_tree, rtvb, offset, "Facer", /*withFlag*/1);
			offset = dissectUruObject(sub_tree, rtvb, offset, "Facee", /*withFlag*/1);
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Dot: %f", tvb_get_letohieee_float(rtvb, offset));
			offset += 4;
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Is enabled: 0x%02X", tvb_get_guint8(rtvb, offset));
			++offset;
			break;
		case proContainedEventData:
			offset = dissectUruObject(sub_tree, rtvb, offset, "Contained", /*withFlag*/1);
			offset = dissectUruObject(sub_tree, rtvb, offset, "Container", /*withFlag*/1);
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Is entering: 0x%02X", tvb_get_guint8(rtvb, offset));
			++offset;
			break;
		case proActivateEventData:
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Is ative: 0x%02X", tvb_get_guint8(rtvb, offset));
			++offset;
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Is activated: 0x%02X", tvb_get_guint8(rtvb, offset));
			++offset;
			break;
		case proResponderStateEventData:
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "State: %d", tvb_get_letohl(rtvb, offset));
			offset += 4;
			break;
		case proMultiStateEventData:
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "State: %d", tvb_get_letohl(rtvb, offset));
			offset += 4;
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Event: %d", tvb_get_letohl(rtvb, offset));
			offset += 4;
			
			offset = dissectUruObject(sub_tree, rtvb, offset, "Avatar", /*withFlag*/1);
			break;
		case proSpawnedEventData:
			offset = dissectUruObject(sub_tree, rtvb, offset, "Spawner", /*withFlag*/1);
			offset = dissectUruObject(sub_tree, rtvb, offset, "Spawnee", /*withFlag*/1);
			break;
		case proOfferLinkBookEventData:
			offset = dissectUruObject(sub_tree, rtvb, offset, "offerer", /*withFlag*/1);
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Target age: %d", tvb_get_letohl(rtvb, offset));
			offset += 4;
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Offeree: %d", tvb_get_letohl(rtvb, offset));
			offset += 4;
			break;
		default:
			uru_parse_error(sub_tree, rtvb, 0, 0, "Unknown Event Data type");
	}
	
	return offset;
}

static guint32 dissectPlasmaObject(proto_tree *sub_tree, tvbuff_t *rtvb, guint32 offset, guint16 type)
{
	guint8 var8;
	guint32 var32, startOffset = offset;
	float varFloat;
	proto_item *tf = proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 0, "Plasma Object");
	sub_tree = proto_item_add_subtree(tf, ett_uru_netmsg_subtree);
	
	if (type == plNull) return offset;
	offset = dissectPlasmaMessage(sub_tree, rtvb, offset);
	
	switch (type) {
		// plLoadCloneMsg and the subclass, plLoadAvatarMsg
		case plLoadCloneMsg:
		case plLoadAvatarMsg:
			// plLoadCloneMsg part
			offset = dissectUruObject(sub_tree, rtvb, offset, "Cloned Object", /*withFlag*/1);
			offset = dissectUruObject(sub_tree, rtvb, offset, "Unknown Object 1", /*withFlag*/1);
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "ID: %d", tvb_get_letohl(rtvb, offset));
			offset += 4;
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Unknown: %d", tvb_get_letohl(rtvb, offset));
			offset += 4;
	
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Unknown: 0x%02X", var8);
			if (var8 != 0x01) uru_parse_error(sub_tree, rtvb, offset, 1, "Invalid value: 0x%02X != 0x01", var8);
			++offset;
	
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Is Load: 0x%02X", tvb_get_guint8(rtvb, offset));
			++offset;
			
			{ // a sub-object
				guint16 subtype = tvb_get_letohs(rtvb, offset);
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Sub Object Type: 0x%04X (%s)", subtype, val_to_str(subtype, plPlasmaObjectTypes, "Unknown"));
				offset += 2;
				offset = dissectPlasmaObject(sub_tree, rtvb, offset, subtype);
			}
			if (type == plLoadCloneMsg) break;
			
			// plLoadAvatarMsg part
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Is Player Avatar: 0x%02X", tvb_get_guint8(rtvb, offset));
			++offset;
			offset = dissectUruObject(sub_tree, rtvb, offset, "Unknown Object 2", /*withFlag*/1);
			
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Unknown: 0x%02X", var8);
			if (var8 != 0x00) uru_parse_error(sub_tree, rtvb, offset, 1, "Invalid value: 0x%02X != 0x00", var8);
			++offset;
			break;
		// known messages
		case plServerReplyMsg:
			var32 = tvb_get_letohl(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Unknown: %d", var32);
			if (var32 != 1) uru_parse_error(sub_tree, rtvb, offset, 4, "Invalid value: %d != 1", var32);
			offset += 4;
			break;
		case plAvatarMsg:
			break;
		case plNotifyMsg:
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Type: %d", tvb_get_letohl(rtvb, offset));
			offset += 4;
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "State: %f", tvb_get_letohieee_float(rtvb, offset));
			offset += 4;
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "ID: %d", tvb_get_letohl(rtvb, offset));
			offset += 4;
			
			{
				guint32 i, count = tvb_get_letohl(rtvb, offset);
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Count: %d", count);
				offset += 4;
				
				for (i = 1; i <= count; ++i)
					offset = dissectProEventData(sub_tree, rtvb, offset, i);
			}
			
			break;
		case plAvatarInputStateMsg:
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "State: %d", tvb_get_letohs(rtvb, offset));
			offset += 2;
			break;
		case plParticleTransferMsg:
			offset = dissectUruObject(sub_tree, rtvb, offset, "Unknown Object", /*withFlag*/1);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Count: %d", tvb_get_letohs(rtvb, offset));
			offset += 2;
			break;
		case plAvBrainGenericMsg:
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Unknown: %d", tvb_get_letohl(rtvb, offset));
			offset += 4;
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Unknown: %d", tvb_get_letohl(rtvb, offset));
			offset += 4;
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Unknown: 0x%02X", tvb_get_guint8(rtvb, offset));
			++offset;
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Unknown: %f", tvb_get_letohieee_float(rtvb, offset));
			offset += 4;
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Unknown: 0x%02X", tvb_get_guint8(rtvb, offset));
			++offset;
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Unknown: 0x%02X", tvb_get_guint8(rtvb, offset));
			++offset;
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Unknown: %f", tvb_get_letohieee_float(rtvb, offset));
			offset += 4;
			break;
		case pfKIMsg:
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Unknown: 0x%02X", var8);
			if (var8 != 0x00) uru_parse_error(sub_tree, rtvb, offset, 1, "Invalid value: 0x%02X != 0x00", var8);
			++offset;
			
			offset += addUStr(rtvb, offset, sub_tree, "Sender Name");
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Sender KI: %d", tvb_get_letohl(rtvb, offset));
			offset += 4;
			offset += addUStr(rtvb, offset, sub_tree, "Message Text");
			
			var32 = tvb_get_letohl(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Message Type: 0x%04X (%s)", var32, val_to_str(var32, plKIMsgTypes, "Unknown"));
			offset += 4;
			
			varFloat = tvb_get_letohieee_float(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Unknown: %f", varFloat);
			if (varFloat != 0.0) uru_parse_error(sub_tree, rtvb, offset, 4, "Invalid value: %f != 0.0", varFloat);
			offset += 4;
			
			var32 = tvb_get_letohl(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Unknown: %d", var32);
			if (var32 != 0) uru_parse_error(sub_tree, rtvb, offset, 4, "Invalid value: %d != 0", var32);
			offset += 4;
			break;
		// message of which the details are unknown
		case plAnimCmdMsg:
		case plControlEventMsg:
		case plEnableMsg:
		case plWarpMsg:
		case plAvTaskMsg:
		case plAvSeekMsg:
		case plAvOneShotMsg:
		case plLinkToAgeMsg:
		case plLinkEffectsTriggerMsg:
		case plOneShotMsg:
		case plParticleKillMsg:
		case plLinkingMgrMsg:
		case plClothingMsg:
		case plInputIfaceMgrMsg:
		case plMultistageModMsg:
		case plBulletMsg:
		case plSubWorldMsg:
		case plClimbMsg:
		case pfMarkerMsg:
		case plAvCoopMsg:
		case plSetNetGroupIDMsg:
		case plPseudoLinkEffectMsg:
		case pfClimbingWallMsg:
			offset = tvb_length(rtvb); // ignore the rest of the content
			break;
		default:
			uru_parse_error(sub_tree, rtvb, 0, 0, "Unknown Plasma Object type");
			offset = tvb_length(rtvb); // ignore the rest of the content
	}
	
	proto_item_set_len(tf, offset-startOffset); // set the correct size
	return offset;
}

static guint32 dissectSdl(proto_tree *sub_tree, tvbuff_t *rtvb, guint32 offset)
{
	tvbuff_t *sub_tvb;
	guint16 version, type;
	gchar str[256];
	// add a subtree
	guint32 startOffset = offset;
	proto_item *tf = proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 0 /* we dont know the size yet */, "SDL");
	sub_tree = proto_item_add_subtree(tf, ett_uru_netmsg_subtree);
	
	offset = dissectStreamedObject(sub_tree, rtvb, offset, &sub_tvb, &type);
	if (sub_tvb) {
		gint innerOffset = 0;
		
		if (type != plNull)
			uru_parse_error(sub_tree, rtvb, 0, 0, "object type must be plNull");

		innerOffset += addUStr2(sub_tvb, innerOffset, sub_tree, "Name", str, 255);
		
		version = tvb_get_letohs(sub_tvb, innerOffset);
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, sub_tvb, innerOffset, 2, "SDL Version: %d", version);
		innerOffset += 2;
		proto_item_append_text(tf, " (%s, version %d)", str, version);
		
		proto_tree_add_none_format(sub_tree, hf_uru_netmsg, sub_tvb, innerOffset, -1, "SDL binary (the SDL file would be needed to dissect this)");
		innerOffset = tvb_length(sub_tvb);
	}
	
	proto_item_set_len(tf, offset-startOffset); // set the correct size
	return offset;
}

static guint32 dissectPageDefinition(proto_tree *sub_tree, tvbuff_t *rtvb, guint32 offset)
{
	gchar str[256];
	// add a subtree
	guint32 startOffset = offset;
	proto_item *tf = proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 0 /* we dont know the size yet */, "Page Definition");
	sub_tree = proto_item_add_subtree(tf, ett_uru_netmsg_subtree);
	
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Page ID: 0x%08X", tvb_get_letohl(rtvb, offset));
	offset += 4;
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Page Type: 0x%04X", tvb_get_letohs(rtvb, offset));
	offset += 2;
	
	offset += addUStr2(rtvb, offset, sub_tree, "Node Name", str, 255);
	proto_item_append_text(tf, " (Node Name: %s)", str);
	
	proto_item_set_len(tf, offset-startOffset); // set the correct size
	return offset;
}

static guint32 dissectPlayerInfo(proto_tree *sub_tree, tvbuff_t *rtvb, guint32 offset)
{
	guint32 ki, var32;
	guint16 var16;
	guint8 var8;
	gchar str[256];
	// add a subtree
	guint32 startOffset = offset;
	proto_item *tf = proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 0 /* we dont know the size yet */, "Player Info");
	sub_tree = proto_item_add_subtree(tf, ett_uru_netmsg_subtree);
	
	var32 = tvb_get_letohl(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Unknown: %d", var32);
	if (var32 != 0x00000020) uru_parse_error(sub_tree, rtvb, offset, 4, "Invalid value: 0x%08X != 0x00000020", var32);
	offset += 4;
	
	var16 = tvb_get_letohs(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "ClientGUID flag: 0x%04X", var16);
	if (var16 != 0x03EA) uru_parse_error(sub_tree, rtvb, offset, 2, "Invalid value: 0x%04X != 0x03EA", var16);
	offset += 2;
	
	ki = tvb_get_letohl(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Player KI: %d", ki);
	offset += 4;
	
	offset += addUStr2(rtvb, offset, sub_tree, "Player Name", str, 255);
	proto_item_append_text(tf, " (KI: %d, Name: %s)", ki, str);
	
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Is CCR: 0x%02X", tvb_get_guint8(rtvb, offset));
	++offset;
	
	var8 = tvb_get_guint8(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Sever Type: 0x%02X", var8);
	if (var8 != 0x03) uru_parse_error(sub_tree, rtvb, offset, 1, "Invalid value: 0x%02X != 0x03", var8);
	++offset;
	
	offset += addIPStr(rtvb, offset, sub_tree, "Client IP", 0 /* not network byte order */);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Client Port: %d", tvb_get_letohs(rtvb, offset));
	offset += 2;
	
	var8 = tvb_get_guint8(rtvb, offset);
	proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Reserved: 0x%02X", var8);
	if (var8 != 0x00) uru_parse_error(sub_tree, rtvb, offset, 1, "Invalid value: 0x%02X != 0x00", var8);
	++offset;
	
	offset = dissectUruObject(sub_tree, rtvb, offset, "Avatar Object", /*withFlag*/0);
	
	proto_item_set_len(tf, offset-startOffset); // set the correct size
	return offset;
}

int dissectNetMsg(proto_tree *sub_tree, guint16 netmsgtype, tvbuff_t *rtvb, guint32 offset)
{
	int i, count;
	guint8 var8;
	guint16 var16;
	guint32 var32;
	switch (netmsgtype) {
		case NetMsgCustomTest: // has no fixed format, so just accept whatever we get
			offset += tvb_length_remaining(rtvb, offset);
			break;
		case NetMsgLeave: // these three seem to have exactly the same body format
		case NetMsgTerminated:
		case NetMsgPlayerTerminated:
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Reason: 0x%02X (%s)", var8, val_to_str(var8, plReasonCodes, "Unknown"));
			++offset;
			break;
		case NetMsgPagingRoom:
			var32 = tvb_get_letohl(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Number of pages: %d", var32);
			if (var32 != 1)
				uru_parse_error(sub_tree, rtvb, offset, 4, "Invalid value: %d != 1", var32);
			offset += 4;
			
			offset = dissectPageDefinition(sub_tree, rtvb, offset);
			
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Is page out: 0x%02X", var8);
			++offset;
			break;
		case NetMsgJoinReq:
			offset += addIPStr(rtvb, offset, sub_tree, "Client IP", 1 /* network byte order */);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Client Port: %d", tvb_get_letohs(rtvb, offset));
			offset += 2;
			break;
		case NetMsgJoinAck:
			var16 = tvb_get_letohs(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Unknown flag (JoinOrder and ExpLevel?): 0x%04X", var16);
			if (var16 != 0x0000) uru_parse_error(sub_tree, rtvb, offset, 2, "Invalid value: 0x%04X != 0x0000", var16);
			offset += 2;
			
			offset = dissectSdl(sub_tree, rtvb, offset);
			break;
		case NetMsgPing:
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 8, "Time: %f", tvb_get_letohieee_double(rtvb, offset));
			offset += 8;
			
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Destination: 0x%02X (%s)", var8, val_to_str(var8, plDestinations, "Unknown"));
			++offset;
			break;
		case NetMsgGroupOwner:
			var32 = tvb_get_letohl(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Format: %d", var32);
			if (var32 != 1) uru_parse_error(sub_tree, rtvb, offset, 4, "Invalid value: %d != 1", var32);
			offset += 4;
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Page ID: 0x%08X", tvb_get_letohl(rtvb, offset));
			offset += 4;
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Page Type: 0x%04X", tvb_get_letohs(rtvb, offset));
			offset += 2;
			
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "GroupID flag: 0x%02X", var8);
			if (var8 != 0x00) uru_parse_error(sub_tree, rtvb, offset, 1, "Invalid value: 0x%02X != 0x00", var8);
			++offset;
			
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Is owned by receiver: 0x%02X", var8);
			++offset;
			break;
		case NetMsgGameStateRequest:
			count = tvb_get_letohl(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Number of explicitly requested pages: %d", count);
			offset += 4;
			
			for (i = 0; i < count; ++i)
				offset = dissectPageDefinition(sub_tree, rtvb, offset);
			break;
		case NetMsgTestAndSet:
			offset = dissectUruObject(sub_tree, rtvb, offset, "Object", /*withFlag*/0);
			
			{
				// add a subtree
				tvbuff_t *sub_tvb;
				guint32 startOffset = offset;
				proto_item *tf = proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 0 /* we dont know the size yet */, "State");
				proto_tree *state_tree = proto_item_add_subtree(tf, ett_uru_netmsg_subtree);
				
				offset = dissectStreamedObject(state_tree, rtvb, offset, &sub_tvb, NULL); // there is no type here...
				if (sub_tvb) {
					guint32 innerOffset = 0;
					
					innerOffset += addUStrChecked(sub_tvb, innerOffset, state_tree, "State Name", "TrigState");
					
					var32 = tvb_get_letohl(sub_tvb, innerOffset);
					proto_tree_add_none_format(state_tree, hf_uru_netmsg, sub_tvb, innerOffset, 4, "Variable Count: %d", var32);
					if (var32 != 1) uru_parse_error(state_tree, sub_tvb, innerOffset, 4, "Invalid value: %d != 1", var32);
					innerOffset += 4;
					
					proto_tree_add_none_format(state_tree, hf_uru_netmsg, sub_tvb, innerOffset, 1, "Server may delete: 0x%02X", tvb_get_guint8(sub_tvb, innerOffset));
					++innerOffset;
					
					innerOffset += addUStrChecked(sub_tvb, innerOffset, state_tree, "Variable Name", "Triggered");
					
					var8 = tvb_get_guint8(sub_tvb, innerOffset);
					proto_tree_add_none_format(state_tree, hf_uru_netmsg, sub_tvb, innerOffset, 1, "Variable Type: %d (2 = boolean)", var8);
					if (var8 != 2) uru_parse_error(state_tree, sub_tvb, innerOffset, 1, "Invalid value: %d != 2", var8);
					++innerOffset;
					
					proto_tree_add_none_format(state_tree, hf_uru_netmsg, sub_tvb, innerOffset, 1, "Variable Value: 0x%02X", tvb_get_guint8(sub_tvb, innerOffset));
					++innerOffset;
					
					if (innerOffset != tvb_length(sub_tvb))
						uru_parse_error(state_tree, sub_tvb, 0, 0, "Size mismatch: %d != %d", innerOffset, tvb_length(sub_tvb));
				}
				proto_item_set_len(tf, offset-startOffset); // set the correct size
			}
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Lock request: 0x%02X", tvb_get_guint8(rtvb, offset));
			++offset;
			
			break;
		case NetMsgMembersList:
			var16 = tvb_get_letohs(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Number of players: %d", var16);
			offset += 2;
			
			for (i = 0; i < var16; ++i) {
				offset = dissectPlayerInfo(sub_tree, rtvb, offset);
			}
			break;
		case NetMsgMemberUpdate:
			offset = dissectPlayerInfo(sub_tree, rtvb, offset);
			
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Member joined: 0x%02X", var8);
			++offset;
			break;
		case NetMsgCreatePlayer:
		case NetMsgCustomVaultCreatePlayer:
			if (netmsgtype == NetMsgCustomVaultCreatePlayer) {
				offset += addUStr(rtvb, offset, sub_tree, "Login");
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Access level: %d", tvb_get_guint8(rtvb, offset));
				++offset;
			}
			offset += addUStr(rtvb, offset, sub_tree, "Avatar");
			offset += addUStr(rtvb, offset, sub_tree, "Gender");
			offset += addUStr(rtvb, offset, sub_tree, "Friend name");
			offset += addUStr(rtvb, offset, sub_tree, "Pass key");
			
			var32 = tvb_get_letohl(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Unknown: %d", var32);
			if (var32 != 0) uru_parse_error(sub_tree, rtvb, offset, 4, "Invalid value: %d != 0", var32);
			offset += 4;
			break;
		case NetMsgDeletePlayer:
			var16 = tvb_get_letohs(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Unknown: 0x%04X", var16);
			if (var16 != 0x0000) uru_parse_error(sub_tree, rtvb, offset, 2, "Invalid value: 0x%04X != 0x0000", var16);
			offset += 2;
			break;
		case NetMsgAuthenticateHello:
			offset += addUStr(rtvb, offset, sub_tree, "Login");
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Maximum packet size: %d", tvb_get_letohs(rtvb, offset));
			offset += 2;
			
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Release: 0x%02X (%s)", var8, val_to_str(var8, plReleases, "Unknown"));
			++offset;
			break;
		case NetMsgAuthenticateChallenge:
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Result: 0x%02X (%s)", var8, val_to_str(var8, plAuthCodes, "Unknown"));
			++offset;
			
			offset += addHexUStr(rtvb, offset, sub_tree, "Challenge");
			break;
		case NetMsgInitialAgeStateSent:
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Number of states sent: %d", tvb_get_letohl(rtvb, offset));
			offset += 4;
			break;
		case NetMsgAuthenticateResponse:
			offset += addHexUStr(rtvb, offset, sub_tree, "Hash");
			break;
		case NetMsgAccountAuthenticated:
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Result: 0x%02X (%s)", var8, val_to_str(var8, plAuthCodes, "Unknown"));
			++offset;
			
			offset += addHex2Ascii(rtvb, offset, 8, sub_tree, "GUID");
			break;
		case NetMsgFindAge:
		case NetMsgCustomVaultFindAge:
			offset = dissectAgeLinkStruct(sub_tree, rtvb, offset);
			break;
		case NetMsgFindAgeReply:
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Flags: 0x%02X", var8);
			if (var8 != 0x1F) uru_parse_error(sub_tree, rtvb, offset, 1, "Invalid value: 0x%02X != 0x1F", var8);
			++offset;
			
			offset += addUStr(rtvb, offset, sub_tree, "Age Name");
			
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Server Type: 0x%02X (%s)", var8, val_to_str(var8, plDestinations, "Unknown"));
			++offset;
			
			offset += addUStr(rtvb, offset, sub_tree, "Server IP");
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Server Port: %d", tvb_get_letohs(rtvb, offset));
			offset += 2;
			
			offset += addHex2Ascii(rtvb, offset, 8, sub_tree, "GUID");
			break;
		case NetMsgSDLState:
		case NetMsgSDLStateBCast:
			offset = dissectUruObject(sub_tree, rtvb, offset, "Object", /*withFlag*/0);
			offset = dissectSdl(sub_tree, rtvb, offset);
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Initial age state: 0x%02X", tvb_get_guint8(rtvb, offset));
			++offset;
			
			if (netmsgtype == NetMsgSDLStateBCast) {
				var8 = tvb_get_guint8(rtvb, offset);
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Persistent on server: 0x%02X", var8);
				if (var8 != 0x01) uru_parse_error(sub_tree, rtvb, offset, 1, "Invalid value: 0x%02X != 0x01", var8);
				++offset;
			}
			break;
		case NetMsgVault:
		case NetMsgVault-1:
		case NetMsgVaultTask:
		{
			guint8 compression;
			guint32 realSize, endOffset;
			
			var8 = tvb_get_guint8(rtvb, offset);
			if (netmsgtype == NetMsgVaultTask)
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Command: 0x%02X (%s)", var8, val_to_str(var8, plVaultTasks, "Unknown"));
			else
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Command: 0x%02X (%s)", var8, val_to_str(var8, plVaultCmds, "Unknown"));
			++offset;
			
			var16 = tvb_get_letohs(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Result code: 0x%04X", var16);
			if (var16 != 0x0000) uru_parse_error(sub_tree, rtvb, offset, 2, "Invalid value: 0x%04X != 0x0000", var16);
			offset += 2;
			
			compression = tvb_get_guint8(rtvb, offset); // compression format
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Compression: 0x%02X (%s)", compression, val_to_str(compression, plVaultZlibs, "Unknown"));
			++offset;
			
			realSize = tvb_get_letohl(rtvb, offset); // size
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Real Size of Data: %d", realSize);
			offset += 4;
			
			{ // the vault items
				tvbuff_t *items_tvb = NULL;
			
				if (compression == 3) { // compressed
					guint32 compSize = tvb_get_letohl(rtvb, offset); // compressed size
					proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Compressed Size of Data: %d", compSize);
					offset += 4;
					endOffset = offset+compSize; // position which is after the items
					
					items_tvb = tvb_uncompress(rtvb, offset, compSize);
					tvb_set_child_real_data_tvbuff(rtvb, items_tvb);
					add_new_data_source(uru_global_pinfo, items_tvb, "Uncompressed Vault Items");
					tvb_set_free_cb(items_tvb, g_free);
					if (tvb_length(items_tvb) != realSize)
						uru_parse_error(sub_tree, items_tvb, 0, 0, "Decompressed size mismatch: %d != %d", tvb_length(items_tvb), realSize);
				}
				else { // uncompressed
					items_tvb = tvb_new_subset(rtvb, offset, realSize, realSize);
					endOffset = offset+realSize;
				}
				offset = 0;
				
				var16 = tvb_get_letohs(items_tvb, offset); // num. items
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, items_tvb, offset, 2, "Number of items: %d", var16);
				offset += 2;
				
				for (i = 1; i <= var16; ++i)
					offset = dissectVaultItem(sub_tree, netmsgtype, i, items_tvb, offset);
				
				if (offset != tvb_length(items_tvb))
					uru_parse_error(sub_tree, items_tvb, 0, 0, "Size mismatch: %d != %d", tvb_length(items_tvb), offset);
			} // we're after the items now
			offset = endOffset;
			
			if (netmsgtype == NetMsgVaultTask) {
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Sub: %d", tvb_get_guint8(rtvb, offset));
				++offset;
				
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Client: %d", tvb_get_letohl(rtvb, offset));
				offset += 4;
			}
			else {
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Context: %d", tvb_get_letohs(rtvb, offset));
				offset += 2;
				
				var16 = tvb_get_letohs(rtvb, offset);
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Result code: 0x%04X", var16);
				if (var16 != 0x0000) uru_parse_error(sub_tree, rtvb, offset, 2, "Invalid value: 0x%04X != 0x0000", var16);
				offset += 2;
				
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "VaultMGR: %d", tvb_get_letohl(rtvb, offset));
				offset += 4;
				
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "VN: %d", tvb_get_letohs(rtvb, offset));
				offset += 2;
			}
			break;
		}
		case NetMsgRelevanceRegions:
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 8, "Regions I care about (bitmask)");
			offset += 8;
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 8, "Regions I\'m in (bitmask)");
			offset += 8;
			break;
		case NetMsgPlayerPage:
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Is page out: 0x%02X", var8);
			++offset;
			
			offset = dissectUruObject(sub_tree, rtvb, offset, "Player Object", /*withFlag*/0);
			break;
		case NetMsgCustomAuthAsk:
			offset += addIPStr(rtvb, offset, sub_tree, "Client IP", 1 /* network byte order */);
			offset += addUStr(rtvb, offset, sub_tree, "Login");
			offset += addHex2Ascii(rtvb, offset, 16, sub_tree, "Challenge");
			offset += addHex2Ascii(rtvb, offset, 16, sub_tree, "Hash");
			
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Release: 0x%02X (%s)", var8, val_to_str(var8, plReleases, "Unknown"));
			++offset;
			break;
		case NetMsgCustomAuthResponse:
			offset += addUStr(rtvb, offset, sub_tree, "Login");
			
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Result: 0x%02X (%s)", var8, val_to_str(var8, plAuthCodes, "Unknown"));
			++offset;
			
			offset += addUStr(rtvb, offset, sub_tree, "Password");
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Access level: %d", tvb_get_guint8(rtvb, offset));
			++offset;
			break;
		case NetMsgVaultPlayerList:
		case NetMsgCustomVaultPlayerList:
			var16 = tvb_get_letohs(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Number of avatars: %d", var16);
			offset += 2;
			
			for (i = 1; i <= var16; ++i) {
				char str[101];
				int startOffset = offset;
				// add avatar tree root
				proto_item *tf = proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 0 /* we don't know the size yet */, "Avatar %d", i);
				proto_tree *avatar_tree = proto_item_add_subtree(tf, ett_uru_netmsg_subtree);
				// add avatar tree elements
				proto_tree_add_none_format(avatar_tree, hf_uru_netmsg, rtvb, offset, 4, "KI: %d", tvb_get_letohl(rtvb, offset));
				offset += 4;
				
				offset += addUStr2(rtvb, offset, avatar_tree, "Avatar", str, 100);
				proto_item_append_text(tf, " (%s)", str);
				
				proto_tree_add_none_format(avatar_tree, hf_uru_netmsg, rtvb, offset, 1, "Flags: 0x%02X", tvb_get_guint8(rtvb, offset));
				++offset;
				
				proto_item_set_len(tf, offset-startOffset); // set the correct size
			}
			
			if (netmsgtype == NetMsgVaultPlayerList)
				offset += addUStr(rtvb, offset, sub_tree, "URL");
			break;
		case NetMsgPython:
			// I saw only one example of this message so far: When linking to the hood with the MOUL heek converted and enabled
			offset += addUStr(rtvb, offset, sub_tree, "Unknown  1");
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Unknown 2: 0x%08X", tvb_get_letohl(rtvb, offset));
			offset += 4;
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Unknown 3: 0x%08X", tvb_get_letohl(rtvb, offset));
			offset += 4;
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Unknown 4: 0x%04X", tvb_get_letohs(rtvb, offset));
			offset += 2;
			offset += addUStr(rtvb, offset, sub_tree, "Unknown 5");
			break;
		case NetMsgSetMyActivePlayer:
			offset += addUStr(rtvb, offset, sub_tree, "Avatar");
			
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Unknown: 0x%02X", var8);
			if (var8 != 0x00) uru_parse_error(sub_tree, rtvb, offset, 1, "Invalid value: 0x%02X != 0x00", var8);
			++offset;
			break;
		case NetMsgSetTimeout-1:
		case NetMsgSetTimeout:
		case NetMsgActivePlayerSet:
			// distinguish NetMsgSetTimeout and NetMsgActivePlayerSet_UU: NetMsgActivePlayerSet_UU doesn't have a message body
			if (netmsgtype <= NetMsgSetTimeout && tvb_length_remaining(rtvb, offset) > 0) {
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Timeout: %f", tvb_get_letohieee_float(rtvb, offset));
				offset += 4;
			}
			break;
		case NetMsgGetPublicAgeList-1:
		case NetMsgGetPublicAgeList:
		case NetMsgPublicAgeList:
		case NetMsgCreatePublicAge:
		case NetMsgPublicAgeCreated:
			/* can not separate these easily:
			 * The GetList command is just a string, so if the first two bytes are the rest of the length, it's all right.
			 * The AgeList: If the 2nd byte is zero, it's an age list of less than 256 ages (2nd byte of an AgeInfoStruct is first byte of age filename,
			 *     not zero, so it's not a CreateAge message). For the really long lists, we also trigger this for really long messages.
			 * The two create age msgs have the same format. */
			var8 = tvb_get_letohs(rtvb, offset+1);
			var16 = tvb_get_letohs(rtvb, offset);
			var32 = tvb_length_remaining(rtvb, offset+2);
			if (netmsgtype <= NetMsgGetPublicAgeList && var32 == var16) {
				// it's the get thingie
				offset += addUStr(rtvb, offset, sub_tree, "Age");
			}
			else if (netmsgtype <= NetMsgPublicAgeList && (var8 == 0 || var32 > 10*255)) { // an age info struct is at least 10 bytes, since it will always contain the GUID
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Number of ages: %d", var16);
				offset += 2;
				for (i = 1; i <= var16; ++i) {
					offset = dissectAgeInfoStruct(sub_tree, rtvb, offset);
				}
				
				var16 = tvb_get_letohs(rtvb, offset);
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Number of people counts: %d", var16);
				offset += 2;
				for (i = 1; i <= var16; ++i) {
					proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "People in age %d: %d", i, tvb_get_letohl(rtvb, offset));
					offset += 4;
				}
			}
			else {
				offset = dissectAgeInfoStruct(sub_tree, rtvb, offset);
			}
			break;
		case NetMsgRemovePublicAge-1:
		case NetMsgRemovePublicAge:
		case NetMsgPublicAgeRemoved:
			offset += addHex2Ascii(rtvb, offset, 8, sub_tree, "GUID");
			break;
		case NetMsgPlayerCreated:
		case NetMsgCustomVaultPlayerCreated:
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Result: 0x%02X (%s)", var8, val_to_str(var8, plCreatePlayerCodes, "Unknown"));
			++offset;
			break;
		case NetMsgCustomVaultDeletePlayer:
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Access level: %d", var8);
			++offset;
			break;
		case NetMsgCustomVaultKiChecked: // this one has the same beginning as the other two
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Status: 0x%02X", tvb_get_guint8(rtvb, offset));
			++offset;
			
			offset += addUStr(rtvb, offset, sub_tree, "Avatar");
			break;
		case NetMsgCustomPlayerStatus:
			offset += addUStr(rtvb, offset, sub_tree, "Account");
			offset += addUStr(rtvb, offset, sub_tree, "Name");
			
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Flag: 0x%02X (%s)", var8, val_to_str(var8, plStatusFlags, "Unknown"));
			++offset;
			
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Status: 0x%02X (%s)", var8, val_to_str(var8, plReasonCodes, "Unknown"));
			++offset;
			break;
		case NetMsgCustomSetGuid:
			offset += addUStr(rtvb, offset, sub_tree, "GUID");
			offset += addUStr(rtvb, offset, sub_tree, "Age filename");
			offset += addUStr(rtvb, offset, sub_tree, "external IP");
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Spawn Start Port: %d", tvb_get_letohs(rtvb, offset));
			offset += 2;
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Spawn Stop Port: %d", tvb_get_letohs(rtvb, offset));
			offset += 2;
			break;
		case NetMsgCustomFindServer:
			offset += addUStr(rtvb, offset, sub_tree, "GUID");
			offset += addUStr(rtvb, offset, sub_tree, "Age filename");
			break;
		case NetMsgCustomServerFound:
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Port: %d", tvb_get_letohs(rtvb, offset));
			offset += 2;
			
			offset += addUStr(rtvb, offset, sub_tree, "IP");
			offset += addUStr(rtvb, offset, sub_tree, "GUID");
			offset += addUStr(rtvb, offset, sub_tree, "Age filename");
			break;
		case NetMsgCustomForkServer:
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 2, "Port: %d", tvb_get_letohs(rtvb, offset));
			offset += 2;
			
			offset += addUStr(rtvb, offset, sub_tree, "GUID");
			offset += addUStr(rtvb, offset, sub_tree, "Age filename");
			
			if (tvb_length_remaining(rtvb, offset)) {
				var8 = tvb_get_guint8(rtvb, offset);
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Load SDL state: %s", var8 ? "Yes" : "No");
				++offset;
			}
			break;
		case NetMsgCustomVaultPlayerStatus:
			offset += addUStr(rtvb, offset, sub_tree, "Age");
			offset += addUStr(rtvb, offset, sub_tree, "GUID");
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Status: 0x%02X", tvb_get_guint8(rtvb, offset));
			++offset;
			
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Online time: %d", tvb_get_letohl(rtvb, offset));
			offset += 4;
			break;
		case NetMsgCustomDirectedFwd:
		case NetMsgGameMessageDirected:
		case NetMsgGameMessage:
		case NetMsgLoadClone:
		{
			tvbuff_t *sub_tvb;
			guint16 type;
			offset = dissectStreamedObject(sub_tree, rtvb, offset, &sub_tvb, &type);
			if (sub_tvb) {
				guint32 innerOffset;
				if (check_col(uru_global_pinfo->cinfo, COL_INFO))
					col_append_fstr(uru_global_pinfo->cinfo, COL_INFO, " (contains %s)", val_to_str(type, plPlasmaObjectTypes, "Unknown"));
				innerOffset = dissectPlasmaObject(sub_tree, sub_tvb, 0, type);
				
				if (innerOffset != tvb_length(sub_tvb))
					uru_parse_error(sub_tree, sub_tvb, 0, 0, "Size mismatch: %d != %d", innerOffset, tvb_length(sub_tvb));
			}
			
			var8 = tvb_get_guint8(rtvb, offset);
			proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Contains timestamp: 0x%02X", var8);
			if (var8 != 0x00) uru_parse_error(sub_tree, rtvb, offset, 1, "Invalid value: 0x%02X != 0x00", var8);
			++offset;
			
			if (netmsgtype == NetMsgLoadClone) {
				offset = dissectUruObject(sub_tree, rtvb, offset, "Cloned Object", /*withFlag*/0);
			
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Player Avatar: 0x%02X", tvb_get_guint8(rtvb, offset));
				++offset;
				
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Clone is loaded: 0x%02X", tvb_get_guint8(rtvb, offset));
				++offset;
				
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Part of initial age state: 0x%02X", tvb_get_guint8(rtvb, offset));
				++offset;
			}
			else if (netmsgtype != NetMsgGameMessage) { // only redirected messages have this part
				var8 = tvb_get_guint8(rtvb, offset);
				proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 1, "Number of recipients: %d", var8);
				++offset;
				
				// show recipients
				for (i = 1; i <= var8; ++i) {
					proto_tree_add_none_format(sub_tree, hf_uru_netmsg, rtvb, offset, 4, "Recipient %d - KI: %d", i, tvb_get_letohl(rtvb, offset));
					offset += 4;
				}
			}
			break;
		}
	}
	return offset;
}
