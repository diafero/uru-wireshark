/*
 * packet-uru.h
 * Routines for Uru dissection
 *
 * Copyright (C) 2005-2006  The Alcugs Project Server Team
 * See the file AUTHORS for more info about the team
 *
 * $Id$
 *
 * Wireshark - Network traffic analyzer
 * By Gerald Combs <gerald@wireshark.org>
 * Copyright 1998 Gerald Combs
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h" // must be the first one to be included

#include <string.h>
#include <gmodule.h>
#include <epan/packet.h>
#include <epan/prefs.h>
#include <epan/reassemble.h>

// for struct in_addr
#include <arpa/inet.h>

#include "uru-prot.h"
// constants which are not in Alcugs
////============================================================================
//// unseen plNetMessage flags defined in earlier server versions
#define plNetFirewalled 0x00000020
#define plNetP2P        0x08000000
////============================================================================
//// proEventData Types
#define proCollisionEventData        1
#define proPickedEventData           2
//#define proControlKeyEventData       3
#define proVariableEventData         4
#define proFacingEventData           5
#define proContainedEventData        6
#define proActivateEventData         7
//#define proCallbackEventData         8
#define proResponderStateEventData   9
#define proMultiStateEventData       10
#define proSpawnedEventData          11
//#define proClickDragEventData        12
//#define proCoopEventData             13
#define proOfferLinkBookEventData    14
//#define proBookEventData             15
//#define proClimbingBlockHitEventData 16
////============================================================================

int dissectNetMsg(proto_tree *sub_tree, guint16 netmsgtype, tvbuff_t *rtvb, guint32 offset);
void uru_parse_error(proto_tree *tree, tvbuff_t *tvb, gint offset, gint length, gchar *text, ...);
