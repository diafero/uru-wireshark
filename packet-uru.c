/*
 * packet-uru.c
 * Routines for Uru dissection
 *
 * Copyright (C) 2005-2006  The Alcugs Project Server Team
 * See the file AUTHORS for more info about the team
 *
 * $Id$
 *
 * Wireshark - Network traffic analyzer
 * By Gerald Combs <gerald@wireshark.org>
 * Copyright 1998 Gerald Combs
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* 
 * http://www.wireshark.org/docs/wsdg_html_chunked/ChDissectAdd.html
 */

#include "packet-uru.h"
#include <epan/expert.h>
#include <stdio.h>


/**
 Decodes the specific packet from Uru validation level 2
   k = offset MOD 8
   dec: x = c * 2 ^ (8-k) MOD 255
*/
void alcDecodePacket2(unsigned char* buf, int n, int offset) {
	int i;
	for(i=0; i<n; i++) {
		buf[i] = buf[i] >> ((i+offset)%8) | buf[i] << (8-((i+offset)%8));
	}
}


/* Forward declarations we need below */
void proto_reg_handoff_uru(void);
static void uru_init_protocol(void);

/* Initialize the subtree pointers */
static gint ett_uru = -1;
static gint ett_header = -1;
static gint ett_netmsgflags = -1;
static gint ett_uru_fragment = -1;
static gint ett_uru_fragments = -1;
static gint ett_netmsg = -1;
gint ett_uru_netmsg_subtree = -1; // used in uru-netmsg.c

/* Set up protocol subtree array */
static gint *ett[] = {
	&ett_uru,
	&ett_header,
	&ett_netmsgflags,
	&ett_uru_fragment,
	&ett_uru_fragments,
	&ett_netmsg,
	&ett_uru_netmsg_subtree
};

/* Initialize the protocol and registered fields */
static int proto_uru = -1;
static int hf_uru_error = -1;
static int hf_uru_pcksize = -1;
static int hf_uru_header = -1;
static int hf_uru_flag = -1;
static int hf_uru_validation_type = -1;
static int hf_uru_checksum = -1;
static int hf_uru_packetnum = -1;
static int hf_uru_msgtype = -1;
static int hf_uru_unkA = -1;
static int hf_uru_fragnum = -1;
static int hf_uru_msgnum = -1;
static int hf_uru_fragct = -1;
static int hf_uru_unkB = -1;
static int hf_uru_fragack = -1;
static int hf_uru_lastack = -1;
static int hf_uru_msglen = -1;
static int hf_uru_bandwidth = -1;
static int hf_uru_nego_ts = -1;
static int hf_uru_nego_sec = -1;
static int hf_uru_nego_usec = -1;
static int hf_uru_ack_frn = -1;
static int hf_uru_ack_sn = -1;
static int hf_uru_ack_frnf = -1;
static int hf_uru_ack_snf = -1;
static int hf_uru_ack = -1;
static int hf_uru_cmd = -1;
static int hf_uru_flags = -1;
static int hf_uru_flags_ts = -1;
static int hf_uru_flags_receivers = -1;
static int hf_uru_flags_timeoutok = -1;
static int hf_uru_flags_firewalled = -1;
static int hf_uru_flags_X = -1;
static int hf_uru_flags_newsdl = -1;
static int hf_uru_flags_statereq1 = -1;
static int hf_uru_flags_ki = -1;
static int hf_uru_flags_relreg = -1;
static int hf_uru_flags_uid = -1;
static int hf_uru_flags_directed = -1;
static int hf_uru_flags_version = -1;
static int hf_uru_flags_system = -1;
static int hf_uru_flags_ack = -1;
static int hf_uru_flags_sid = -1;
static int hf_uru_flags_p2p = -1;
static int hf_uru_version = -1;
static int hf_uru_maxversion = -1;
static int hf_uru_minversion = -1;
static int hf_uru_ts = -1;
static int hf_uru_ts_sec = -1;
static int hf_uru_ts_usec = -1;
static int hf_uru_X = -1;
static int hf_uru_KI = -1;
static int hf_uru_UID = -1;
static int hf_uru_sid = -1;
int hf_uru_netmsg = -1; // used in uru-netmsg.c

/* fields for fragment assembly */
static int hf_uru_fragments = -1;
static int hf_uru_fragment = -1;
static int hf_uru_fragment_overlap = -1;
static int hf_uru_fragment_overlap_conflicts = -1;
static int hf_uru_fragment_multiple_tails = -1;
static int hf_uru_fragment_too_long_fragment = -1;
static int hf_uru_fragment_error = -1;
static int hf_uru_fragment_count = -1;
static int hf_uru_reassembled_in = -1;
static int hf_uru_reassembled_length = -1;

static const fragment_items uru_frag_items = {
	/* Fragment subtrees */
	&ett_uru_fragment,
	&ett_uru_fragments,
	/* Fragment fields */
	&hf_uru_fragments,
	&hf_uru_fragment,
	&hf_uru_fragment_overlap,
	&hf_uru_fragment_overlap_conflicts,
	&hf_uru_fragment_multiple_tails,
	&hf_uru_fragment_too_long_fragment,
	&hf_uru_fragment_error,
	&hf_uru_fragment_count,
	/* Reassembled in field */
	&hf_uru_reassembled_in,
	/* Reassembled length field */
	&hf_uru_reassembled_length,
	/* Tag */
	"Message fragments"
};


static const value_string validtypenames[] = {
	{ 0, "none" },
	{ 1, "checksum" },
	{ 2, "encoded" },
	{ 0, NULL }
};

static const value_string messagetypes[] = {
	{ 0x80, "Ack" },
	{ 0x90, "Ack|Ext" },
	{ 0x42, "Negotiation|AckRequired" },
	{ 0x52, "Negotiation|AckRequired|Ext" },
	{ 0x00, "NetMsg" },
	{ 0x10, "NetMsg|Ext" },
	{ 0x02, "NetMsg|AckRequired" },
	{ 0x12, "NetMsg|AckRequired|Ext" },
	{ 0, NULL }
};

static const value_string messagetypes_short[] = {
	{ 0x80, "Ack   " },
	{ 0x90, "Ack   " },
	{ 0x42, "Nego  " },
	{ 0x52, "Nego  " },
	{ 0x00, "NetMsg" },
	{ 0x10, "NetMsg" },
	{ 0x02, "NetMsg" },
	{ 0x12, "NetMsg" },
	{ 0, NULL }
};

static const value_string plNetMsgs[] = {
	// double-value cmds
	{ NetMsgSetTimeout, "NetMsgSetTimeout | NetMsgActivePlayerSet (UU)" },
	{ NetMsgGetPublicAgeList, "NetMsgGetPublicAgeList | NetMsgPublicAgeList (UU)" },
	{ NetMsgPublicAgeList, "NetMsgPublicAgeList | NetMsgCreatePublicAge (UU)" },
	{ NetMsgCreatePublicAge, "NetMsgCreatePublicAge | NetMsgPublicAgeCreated (UU)" },
	{ NetMsgRemovePublicAge, "NetMsgRemovePublicAge | NetMsgPublicAgeRemoved (UU)" },
	
	// "normal" cmds (plus the ones just for filtering)
	{ NetMsgPagingRoom, "NetMsgPagingRoom" },
	{ NetMsgJoinReq, "NetMsgJoinReq" },
	{ NetMsgJoinAck, "NetMsgJoinAck" },
	{ NetMsgLeave, "NetMsgLeave" },
	{ NetMsgPing, "NetMsgPing" },
	{ NetMsgGroupOwner, "NetMsgGroupOwner" },
	{ NetMsgGameStateRequest, "NetMsgGameStateRequest" },
	{ NetMsgGameMessage, "NetMsgGameMessage" },
	{ NetMsgVoice, "NetMsgVoice" },
	{ NetMsgTestAndSet, "NetMsgTestAndSet" },
	{ NetMsgMembersListReq, "NetMsgMembersListReq" },
	{ NetMsgMembersList, "NetMsgMembersList" },
	{ NetMsgMemberUpdate, "NetMsgMemberUpdate" },
	{ NetMsgCreatePlayer, "NetMsgCreatePlayer" },
	{ NetMsgAuthenticateHello, "NetMsgAuthenticateHello" },
	{ NetMsgAuthenticateChallenge, "NetMsgAuthenticateChallenge" },
	{ NetMsgInitialAgeStateSent, "NetMsgInitialAgeStateSent" },
	{ NetMsgVaultTask, "NetMsgVaultTask" },
	{ NetMsgAlive, "NetMsgAlive" },
	{ NetMsgTerminated, "NetMsgTerminated" },
	{ NetMsgSDLState, "NetMsgSDLState" },
	{ NetMsgSDLStateBCast, "NetMsgSDLStateBCast" },
	{ NetMsgGameMessageDirected, "NetMsgGameMessageDirected" },
	{ NetMsgRequestMyVaultPlayerList, "NetMsgRequestMyVaultPlayerList" },
	{ NetMsgVaultPlayerList, "NetMsgVaultPlayerList" },
	{ NetMsgSetMyActivePlayer, "NetMsgSetMyActivePlayer" },
	{ NetMsgPlayerCreated, "NetMsgPlayerCreated" },
	{ NetMsgFindAge, "NetMsgFindAge" },
	{ NetMsgFindAgeReply, "NetMsgFindAgeReply" },
	{ NetMsgDeletePlayer, "NetMsgDeletePlayer" },
	{ NetMsgAuthenticateResponse, "NetMsgAuthenticateResponse" },
	{ NetMsgAccountAuthenticated, "NetMsgAccountAuthenticated" },
	{ NetMsgRelevanceRegions, "NetMsgRelevanceRegions" },
	{ NetMsgLoadClone, "NetMsgLoadClone" },
	{ NetMsgPlayerPage, "NetMsgPlayerPage" },
	{ NetMsgVault-1, "NetMsgVault (UU)" },
	{ NetMsgVault, "NetMsgVault" },
	{ NetMsgPython-1, "NetMsgPython (UU)" },
	{ NetMsgPython, "NetMsgPython" },
	{ NetMsgSetTimeout-1, "NetMsgSetTimeout (UU)" },
	{ NetMsgSetTimeout, "NetMsgSetTimeout" },
	{ NetMsgActivePlayerSet-1, "NetMsgActivePlayerSet (UU)" },
	{ NetMsgActivePlayerSet, "NetMsgActivePlayerSet" },
	{ NetMsgGetPublicAgeList-1, "NetMsgGetPublicAgeList (UU)" },
	{ NetMsgGetPublicAgeList, "NetMsgGetPublicAgeList" },
	{ NetMsgPublicAgeList-1, "NetMsgPublicAgeList (UU)" },
	{ NetMsgPublicAgeList, "NetMsgPublicAgeList" },
	{ NetMsgCreatePublicAge-1, "NetMsgCreatePublicAge (UU)" },
	{ NetMsgCreatePublicAge, "NetMsgCreatePublicAge" },
	{ NetMsgPublicAgeCreated-1, "NetMsgPublicAgeCreated (UU)" },
	{ NetMsgPublicAgeCreated, "NetMsgPublicAgeCreated" },
	{ NetMsgRemovePublicAge-1, "NetMsgRemovePublicAge (UU)" },
	{ NetMsgRemovePublicAge, "NetMsgRemovePublicAge" },
	{ NetMsgPublicAgeRemoved-1, "NetMsgPublicAgeRemoved (UU)" },
	{ NetMsgPublicAgeRemoved, "NetMsgPublicAgeRemoved" },
	
	{ NetMsgCustomAuthAsk, "NetMsgCustomAuthAsk" },
	{ NetMsgCustomAuthResponse, "NetMsgCustomAuthResponse" },
	{ NetMsgCustomVaultAskPlayerList, "NetMsgCustomVaultAskPlayerList" },
	{ NetMsgCustomVaultPlayerList, "NetMsgCustomVaultPlayerList" },
	{ NetMsgCustomVaultCreatePlayer, "NetMsgCustomVaultCreatePlayer" },
	{ NetMsgCustomVaultPlayerCreated, "NetMsgCustomVaultPlayerCreated" },
	{ NetMsgCustomVaultDeletePlayer, "NetMsgCustomVaultDeletePlayer" },
	{ NetMsgCustomPlayerStatus, "NetMsgCustomPlayerStatus" },
	{ NetMsgCustomVaultCheckKi, "NetMsgCustomVaultCheckKi" },
	{ NetMsgCustomVaultKiChecked, "NetMsgCustomVaultKiChecked" },
	{ NetMsgCustomRequestAllPlStatus, "NetMsgCustomRequestAllPlStatus" },
	{ NetMsgCustomAllPlayerStatus, "NetMsgCustomAllPlayerStatus" },
	{ NetMsgCustomSetGuid, "NetMsgCustomSetGuid" },
	{ NetMsgCustomFindServer, "NetMsgCustomFindServer" },
	{ NetMsgCustomServerFound, "NetMsgCustomServerFound" },
	{ NetMsgCustomForkServer, "NetMsgCustomForkServer" },
	{ NetMsgPlayerTerminated, "NetMsgPlayerTerminated" },
	{ NetMsgCustomVaultPlayerStatus, "NetMsgCustomVaultPlayerStatus" },
	{ NetMsgCustomMetaRegister, "NetMsgCustomMetaRegister" },
	{ NetMsgCustomMetaPing, "NetMsgCustomMetaPing" },
	{ NetMsgCustomServerVault, "NetMsgCustomServerVault" },
	{ NetMsgCustomServerVaultTask, "NetMsgCustomServerVaultTask" },
	{ NetMsgCustomSaveGame, "NetMsgCustomSaveGame" },
	{ NetMsgCustomLoadGame, "NetMsgCustomLoadGame" },
	{ NetMsgCustomCmd, "NetMsgCustomCmd" },
	{ NetMsgCustomDirectedFwd, "NetMsgCustomDirectedFwd" },
	{ NetMsgCustomPlayerToCome, "NetMsgCustomPlayerToCome" },
	{ NetMsgCustomVaultFindAge, "NetMsgCustomVaultFindAge" },
	{ NetMsgCustomTest, "NetMsgCustomTest" },
	{ 0, NULL }
};

static const true_false_string yes_no = {
	"Yes",
	"No"
};

static hf_register_info hf[] = {
	{ &hf_uru_error,
		{ "Parse error", "uru.error",
		FT_BOOLEAN, BASE_NONE, NULL, 0x0,
		"There was an error while parsing", HFILL }
	},
	{ &hf_uru_pcksize,
		{ "Packet size", "uru.pcksize",
		FT_UINT32, BASE_DEC, NULL, 0x0,
		"Size of the whole packet", HFILL }
	},
	{ &hf_uru_header,
		{ "Uru Header", "uru.header",
		FT_NONE, BASE_NONE, NULL, 0x0,
		"The Uru header", HFILL }
	},
	{ &hf_uru_flag,
		{ "Uru Protocol ID", "uru.id",
		FT_UINT8, BASE_HEX, NULL, 0x0,
		"The Uru protocol magic number", HFILL }
	},
	{ &hf_uru_validation_type,
		{ "Uru Validation Type", "uru.valtype",
		FT_UINT8, BASE_DEC, VALS(validtypenames), 0x0,
		"The message validation type", HFILL }
	},
	{ &hf_uru_checksum,
		{ "Uru Checksum", "uru.cksum",
		FT_UINT32, BASE_HEX, NULL, 0x0,
		"32-bit checksum", HFILL }
	},
	/* info gleaned from uru_get_header(unet3) => tUnetUruMsg::store(unet3+) */
	{ &hf_uru_packetnum,
		{ "Packet Number", "uru.pn",
		FT_UINT32, BASE_DEC, NULL, 0x0,
		"Packet counter (unique)", HFILL }
	},
	{ &hf_uru_msgtype,
		{ "Message Type Flags", "uru.tf",
		/*FT_BYTES, BASE_HEX, NULL, 0x0,*/
		FT_UINT8, BASE_HEX, VALS(messagetypes), 0x0,
		"Specifies the message type and whether an ack is required", HFILL }
	},
	{ &hf_uru_unkA,
		{ "Unknown A", "uru.unka",
		FT_UINT32, BASE_DEC, NULL, 0x0,
		"Unknown (should be 0)", HFILL }
	},
	{ &hf_uru_fragnum,
		{ "Fragment Number", "uru.frn",
		FT_UINT8, BASE_DEC, NULL, 0x0,
		"Which fragment this packet is", HFILL }
	},
	{ &hf_uru_msgnum,
		{ "Message Number", "uru.sn",
		FT_UINT24, BASE_DEC, NULL, 0x0,
		"The sequence number of the message this packet belongs to", HFILL }
	},
	{ &hf_uru_fragct,
		{ "Fragment Count", "uru.frt",
		FT_UINT8, BASE_DEC, NULL, 0x0,
		"The number of fragments in the message this packet belongs to", HFILL }
	},
	{ &hf_uru_unkB,
		{ "Unknown B", "uru.unkb",
		FT_UINT32, BASE_DEC, NULL, 0x0,
		"Unknown (should be 0)", HFILL }
	},
	{ &hf_uru_fragack,
		{ "Previous Fragment Ack Required", "uru.pfr",
		FT_UINT8, BASE_DEC, NULL, 0x0,
		"The fragment number of the previous packet requiring an ack", HFILL }
	},
	{ &hf_uru_lastack,
		{ "Previous Ack Required", "uru.ps",
		FT_UINT24, BASE_DEC, NULL, 0x0,
		"The previous packet requiring an ack", HFILL }
	},
	{ &hf_uru_msglen,
		{ "Packet Length", "uru.size",
		FT_UINT32, BASE_DEC, NULL, 0x0,
		"The length of the packet, or the number of acks for Ack messages", HFILL }
	},
	/* data next */
	
	/* 0x42: negotiations: parse_negotiation(unet3)
		=> tmNetClientComm::store(unet3+) */
	{ &hf_uru_bandwidth,
		{ "Bandwidth", "uru.nego.bw",
		FT_UINT32, BASE_DEC, NULL, 0x0,
		"", HFILL }
	},
	{ &hf_uru_nego_ts,
		{ "Timestamp", "uru.nego.ts",
		FT_BYTES, BASE_NONE, NULL, 0x0,
		"", HFILL }
	},
	{ &hf_uru_nego_sec,
		{ "Timestamp (sec)", "uru.nego.sec",
		FT_UINT32, BASE_DEC, NULL, 0x0,
		"", HFILL }
	},
	{ &hf_uru_nego_usec,
		{ "Timestamp (microsec)", "uru.nego.usec",
		FT_UINT32, BASE_DEC, NULL, 0x0,
		"", HFILL }
	},
	
	/* 0x80: acks: ack_update(unet3) => ackCheck(unet3+) */
	{ &hf_uru_ack_frn,
		{ "Acked fragment number", "uru.ack.frn",
		FT_UINT8, BASE_DEC, NULL, 0x0,
		"", HFILL }
	},
	{ &hf_uru_ack_sn,
		{ "Acked message number", "uru.ack.sn",
		FT_UINT24, BASE_DEC, NULL, 0x0,
		"", HFILL }
	},
	{ &hf_uru_ack_frnf,
		{ "Previous acked fragment number", "uru.ack.frnf",
		FT_UINT8, BASE_DEC, NULL, 0x0,
		"", HFILL }
	},
	{ &hf_uru_ack_snf,
		{ "Previous acked message number", "uru.ack.snf",
		FT_UINT24, BASE_DEC, NULL, 0x0,
		"", HFILL }
	},
	{ &hf_uru_ack,
		{ "Ack", "uru.ack",
		FT_NONE, BASE_NONE, NULL, 0x0,
		"", HFILL }
	},
	
	/* 0x02, 0x00: info gleaned from parse_plNet_msg(unet3)
		=> tmMsgBase::store(unet3+) */
	{ &hf_uru_cmd,
		{ "Command", "uru.cmd",
		FT_UINT16, BASE_HEX, VALS(plNetMsgs), 0x0,
		"Which kind of NetMsg this is", HFILL }
	},
	{ &hf_uru_flags,
		{ "NetMsg Flags", "uru.flags",
		FT_UINT32, BASE_HEX, NULL, 0x0,
		"Flags for what is included in the NetMsg header", HFILL }
	},
	{ &hf_uru_flags_ts,
		{ "Timestamp included", "uru.flags.ts",
		FT_BOOLEAN, 32, TFS(&yes_no), plNetTimestamp,
		"", HFILL }
	},
	{ &hf_uru_flags_receivers,
		{ "Receivers", "uru.flags.receivers",
		FT_BOOLEAN, 32, TFS(&yes_no), plNetMsgRecvrs,
		"", HFILL }
	},
	{ &hf_uru_flags_timeoutok,
		{ "IP address included", "uru.flags.timeoutok",
		FT_BOOLEAN, 32, TFS(&yes_no), plNetTimeoutOk,
		"", HFILL }
	},
	{ &hf_uru_flags_firewalled,
		{ "Firewalled", "uru.flags.firewalled",
		FT_BOOLEAN, 32, TFS(&yes_no), plNetFirewalled,
		"", HFILL }
	},
	{ &hf_uru_flags_X,
		{ "X included", "uru.flags.X",
		FT_BOOLEAN, 32, TFS(&yes_no), plNetX,
		"", HFILL }
	},
	{ &hf_uru_flags_newsdl,
		{ "New SDL", "uru.flags.newsdl",
		FT_BOOLEAN, 32, TFS(&yes_no), plNetNewSDL,
		"", HFILL }
	},
	{ &hf_uru_flags_statereq1,
		{ "Initial State Request", "uru.flags.statereq1",
		FT_BOOLEAN, 32, TFS(&yes_no), plNetStateReq1,
		"", HFILL }
	},
	{ &hf_uru_flags_ki,
		{ "KI included", "uru.flags.ki",
		FT_BOOLEAN, 32, TFS(&yes_no), plNetKi,
		"", HFILL }
	},
	{ &hf_uru_flags_relreg,
		{ "Relevance Regions", "uru.flags.relregions",
		FT_BOOLEAN, 32, TFS(&yes_no), plNetRelRegions,
		"", HFILL }
	},
	{ &hf_uru_flags_uid,
		{ "UID included", "uru.flags.uid",
		FT_BOOLEAN, 32, TFS(&yes_no), plNetUID,
		"", HFILL }
	},
	{ &hf_uru_flags_directed,
		{ "Directed", "uru.flags.directed",
		FT_BOOLEAN, 32, TFS(&yes_no), plNetDirected,
		"", HFILL }
	},
	{ &hf_uru_flags_version,
		{ "Version included", "uru.flags.version",
		FT_BOOLEAN, 32, TFS(&yes_no), plNetVersion,
		"", HFILL }
	},
	{ &hf_uru_flags_system,
		{ "System", "uru.flags.system",
		FT_BOOLEAN, 32, TFS(&yes_no), plNetSystem,
		"", HFILL }
	},
	{ &hf_uru_flags_ack,
		{ "Ack required", "uru.flags.ack",
		FT_BOOLEAN, 32, TFS(&yes_no), plNetAck,
		"", HFILL }
	},
	{ &hf_uru_flags_sid,
		{ "SID included", "uru.flags.sid",
		FT_BOOLEAN, 32, TFS(&yes_no), plNetSid,
		"", HFILL }
	},
	{ &hf_uru_flags_p2p,
		{ "P2P", "uru.flags.p2p",
		FT_BOOLEAN, 32, TFS(&yes_no), plNetP2P,
		"", HFILL }
	},
	{ &hf_uru_version,
		{ "Version info", "uru.version",
		FT_NONE, BASE_NONE, NULL, 0x0,
		"", HFILL }
	},
	{ &hf_uru_maxversion,
		{ "Max version", "uru.maxv",
		FT_UINT8, BASE_DEC, NULL, 0x0,
		"", HFILL }
	},
	{ &hf_uru_minversion,
		{ "Min version", "uru.minv",
		FT_UINT8, BASE_DEC, NULL, 0x0,
		"", HFILL }
	},
	{ &hf_uru_ts,
		{ "Timestamp", "uru.ts",
		/* FT_RELATIVE_TIME doesn't work */
		/*FT_RELATIVE_TIME, BASE_DEC, NULL, 0x0,*/
		FT_BYTES, BASE_NONE, NULL, 0x0,
		"", HFILL }
	},
	{ &hf_uru_ts_sec,
		{ "Timestamp (sec)", "uru.ts.sec",
		FT_UINT32, BASE_DEC, NULL, 0x0,
		"", HFILL }
	},
	{ &hf_uru_ts_usec,
		{ "Timestamp (microsec)", "uru.ts.usec",
		FT_UINT32, BASE_DEC, NULL, 0x0,
		"", HFILL }
	},
	{ &hf_uru_X,
		{ "X", "uru.x",
		FT_UINT32, BASE_DEC, NULL, 0x0,
		"", HFILL }
	},
	{ &hf_uru_KI,
		{ "KI", "uru.ki",
		FT_UINT32, BASE_DEC, NULL, 0x0,
		"KI number", HFILL }
	},
	{ &hf_uru_UID,
		{ "UID", "uru.uid",
		FT_BYTES, BASE_NONE, NULL, 0x0,
		"Player UID", HFILL }
	},
	{ &hf_uru_sid, /* custom flag (see unet3+) */
		{ "SID", "uru.sid",
		FT_UINT32, BASE_DEC, NULL, 0x0,
		"", HFILL }
	},
	{ &hf_uru_netmsg,
		{ "Message Body", "uru.netmsg",
		FT_NONE, BASE_NONE, NULL, 0x0,
		"", HFILL }
	},
	
	/* for fragment reassmbly */
	{&hf_uru_fragments,
		{"Message fragments", "uru.fragments",
		FT_NONE, BASE_NONE, NULL, 0x00, NULL, HFILL } },
	{&hf_uru_fragment,
		{"Message fragment", "uru.fragment",
		FT_FRAMENUM, BASE_NONE, NULL, 0x00, NULL, HFILL } },
	{&hf_uru_fragment_overlap,
		{"Message fragment overlap", "uru.fragment.overlap",
		FT_BOOLEAN, 0, NULL, 0x00, NULL, HFILL } },
	{&hf_uru_fragment_overlap_conflicts,
		{"Message fragment overlapping with conflicting data",
		"uru.fragment.overlap.conflicts",
		FT_BOOLEAN, 0, NULL, 0x00, NULL, HFILL } },
	{&hf_uru_fragment_multiple_tails,
		{"Message has multiple tail fragments",
		"uru.fragment.multiple_tails",
		FT_BOOLEAN, 0, NULL, 0x00, NULL, HFILL } },
	{&hf_uru_fragment_too_long_fragment,
		{"Message fragment too long", "uru.fragment.too_long_fragment",
		FT_BOOLEAN, 0, NULL, 0x00, NULL, HFILL } },
	{&hf_uru_fragment_error,
		{"Message defragmentation error", "uru.fragment.error",
		FT_FRAMENUM, BASE_NONE, NULL, 0x00, NULL, HFILL } },
	{&hf_uru_fragment_count,
		{"Message fragment count", "uru.fragment.count",
		FT_UINT32, BASE_DEC, NULL, 0x00, NULL, HFILL } },
	{&hf_uru_reassembled_in,
		{"Reassembled in", "uru.reassembled.in",
		FT_FRAMENUM, BASE_NONE, NULL, 0x00, NULL, HFILL } },
	{&hf_uru_reassembled_length,
		{"Reassembled length", "uru.reassembled.length",
		FT_UINT32, BASE_DEC, NULL, 0x00, NULL, HFILL } },
};


/* Options */
/* port range logic borrowed from plugins/packet-asn1.c */
#define URU_PORT_LOW 5000
#define URU_PORT_HIGH 6000
static range_t *global_uru_port_range;
static range_t *uru_port_range;
static gboolean global_uru_header_style = TRUE;
static gboolean global_uru_summary_ack = FALSE;
static dissector_handle_t uru_handle;
static gboolean uru_handle_inited = FALSE;

/* Fragment reassembly */
static GHashTable *uru_fragment_table = NULL;
static GHashTable *uru_reassembled_table = NULL;

/* Global element to be used by uru_parse_error */
packet_info *uru_global_pinfo = NULL;

void uru_parse_error(proto_tree *tree, tvbuff_t *tvb, gint offset, gint length, gchar *text, ...)
{
	proto_item *tf = NULL;
	
	va_list ap;
	char buffer[1024];
	va_start(ap, text);
	vsnprintf(buffer, 1024, text, ap);
	va_end(ap);
	
	tf = proto_tree_add_boolean(tree, hf_uru_error, tvb, offset, length, TRUE);
	proto_item_set_text(tf, "Error: %s", buffer);
	expert_add_info_format(uru_global_pinfo, tf, PI_MALFORMED, PI_ERROR, "%s", buffer);
	PROTO_ITEM_SET_GENERATED(tf);
	
	if (check_col(uru_global_pinfo->cinfo, COL_INFO))
		col_append_fstr(uru_global_pinfo->cinfo, COL_INFO, " [Error: %s]", buffer);
}

/* Code to actually dissect the packets */
static void dissect_uru(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree)
{
	/* Set up structures needed to add the protocol subtree and manage it */
	proto_item *ti = NULL;
	proto_tree *uru_tree = NULL;
	
	gint offset = 0;
	proto_item *tf, *headerItem = 0;
	proto_tree *sub_tree = NULL;
	
	guint8 packet_type;
	guint32 packetnum, seqnum, lastack, msglen;
	guint8 valtype, flags, fragnum, fragct, fragack;
	guint16 netmsgtype;
	guint32 netmsgflags;
	
	uru_global_pinfo = pinfo;
	
	packet_type = tvb_get_guint8(tvb, 0);
	if (packet_type == 0x3) {
		if (check_col(pinfo->cinfo, COL_PROTOCOL)) {
			col_set_str(pinfo->cinfo, COL_PROTOCOL, "Uru");
		}
		if (check_col(pinfo->cinfo, COL_INFO)) {
			col_clear(pinfo->cinfo, COL_INFO);
			if (pinfo->ptype == PT_UDP) {
				col_add_fstr(pinfo->cinfo, COL_INFO, "%5u->%5u",
						pinfo->srcport, pinfo->destport);
			}
			else {
				col_add_str(pinfo->cinfo, COL_INFO, "bad port type, not UDP");
			}
		}
	}
  else {
#if 0
	if (check_col(pinfo->cinfo, COL_PROTOCOL)) {
		col_set_str(pinfo->cinfo, COL_PROTOCOL, "NOT Uru!");
	}
#endif
	return;
	}

	if (tree) { /* we are being asked for details */
		proto_item *item;
		/* create display subtree for the protocol */
		ti = proto_tree_add_item(tree, proto_uru, tvb, 0, -1, TRUE);
		uru_tree = proto_item_add_subtree(ti, ett_uru);
		item = proto_tree_add_uint(uru_tree, hf_uru_pcksize, tvb, 0, 0, tvb_length_remaining(tvb, 0));
		PROTO_ITEM_SET_GENERATED(item);
	}
	valtype = tvb_get_guint8(tvb, offset+1);
	if (valtype <= 2) { // valtype is unsisnged, so it will always be >= 0
		guint32 chksum, bufsize;
		guint8 *newbuf;
		/*
		Don't fetch a little-endian value using "tvb_get_ntohs() or
		"tvb_get_ntohl()" and then using "g_ntohs()", "g_htons()", "g_ntohl()",
		or "g_htonl()" on the resulting value - the g_ routines in question
		convert between network byte order (big-endian) and *host* byte order,
		not *little-endian* byte order; not all machines on which Wireshark runs
		are little-endian, even though PC's are.  Fetch those values using
		"tvb_get_letohs()" and "tvb_get_letohl()".
		*/
		if (tree) {
			headerItem = proto_tree_add_item(uru_tree, hf_uru_header, tvb, offset, 0, TRUE);
			sub_tree = proto_item_add_subtree(headerItem, ett_header);
			proto_tree_add_item(sub_tree, hf_uru_flag, tvb, offset, 1, TRUE);
			proto_tree_add_item(sub_tree, hf_uru_validation_type, tvb,
					offset+1, 1, TRUE);
		}
		offset += 2;
		if (valtype == 1 || valtype == 2) {
			chksum = tvb_get_letohl(tvb, offset);
			if (tree) {
				proto_tree_add_uint(sub_tree, hf_uru_checksum, tvb, offset, 4, chksum);
				/* TODO: here verify the checksum */
				/* note business of swapping validation types!  ??? */
			}
			offset += 4;
		}
		if (valtype == 2) {
			tvbuff_t *sub_tvb;
			/* the rest of the packet is (might be?) encoded */
			bufsize = tvb_length_remaining(tvb, offset);
			newbuf = tvb_memdup(tvb, offset, bufsize);
			alcDecodePacket2(newbuf, bufsize, offset);
			sub_tvb = tvb_new_real_data(newbuf, bufsize, bufsize);
			tvb_set_child_real_data_tvbuff(tvb, sub_tvb);
			add_new_data_source(pinfo, sub_tvb, "Decoded Data");
			tvb_set_free_cb(sub_tvb, g_free);
			tvb = sub_tvb;
		}
		else {
			tvb = tvb_new_subset(tvb, offset, -1, -1);
		}
	}
	else {
		/* unknown validation type, shouldn't happen */
		if (tree) {
			proto_tree_add_uint_format(sub_tree, hf_uru_validation_type, tvb,
					offset, 1, valtype,
					"Unknown validation type %u", valtype);
		}
		return;
	}
	if (headerItem) proto_item_set_len(headerItem, offset); // set the correct size
	offset = 0;

	/* Uru header */
	packetnum = tvb_get_letohl(tvb, offset);
	if (tree) {
		proto_tree_add_item(sub_tree, hf_uru_packetnum, tvb, offset, 4, TRUE);
	}
	offset += 4;
	flags = tvb_get_guint8(tvb, offset);
	if (tree) {
		proto_tree_add_item(sub_tree, hf_uru_msgtype, tvb, offset, 1, TRUE);
	}
	offset += 1;
	if (!(flags & UNetExt)) {
		if (tree) {
			guint32 unk = tvb_get_letohl(tvb, offset);
			tf = proto_tree_add_item(sub_tree, hf_uru_unkA, tvb, offset, 4, TRUE);
			if (unk == 0) {
				PROTO_ITEM_SET_HIDDEN(tf);
			}
		}
		offset += 4;
	}
	fragnum = tvb_get_guint8(tvb, offset);
	seqnum = tvb_get_letoh24(tvb, offset+1);
	if (tree) {
		proto_tree_add_item(sub_tree, hf_uru_fragnum, tvb, offset, 1, TRUE);
		proto_tree_add_item(sub_tree, hf_uru_msgnum, tvb, offset+1, 3, TRUE);
	}
	offset += 4;
	fragct = tvb_get_guint8(tvb, offset);
	if (tree) {
		proto_tree_add_item(sub_tree, hf_uru_fragct, tvb, offset, 1, TRUE);
	}
	offset += 1;
	if (!(flags & UNetExt)) {
		if (tree) {
			guint32 unk = tvb_get_letohl(tvb, offset);
			tf = proto_tree_add_item(sub_tree, hf_uru_unkB, tvb, offset, 4, TRUE);
			if (unk == 0) {
				PROTO_ITEM_SET_HIDDEN(tf);
			}
		}
		offset+= 4;
	}
	fragack = tvb_get_guint8(tvb, offset);
	lastack = tvb_get_letoh24(tvb, offset+1);
	if (tree) {
		proto_tree_add_item(sub_tree, hf_uru_fragack, tvb, offset, 1, TRUE);
		proto_tree_add_item(sub_tree, hf_uru_lastack, tvb, offset+1, 3, TRUE);
	}
	offset += 4;
	msglen = tvb_get_letohl(tvb, offset);
	if (tree) {
		guint32 size = msglen;
		proto_tree_add_item(sub_tree, hf_uru_msglen, tvb, offset, 4, TRUE);
		
		// verify this number
		if (flags == UNetAckReply)
			size = (msglen*16)+2;
		else if (flags == (UNetAckReply|UNetExt))
			size = msglen*8;
		if ((guint32)tvb_length_remaining(tvb, offset+4) != size) {
			uru_parse_error(sub_tree, tvb, offset, 4, "Message length is wrong");
			return;
		}
	}
	offset += 4;
	if (headerItem) proto_item_set_len(headerItem, proto_item_get_len(headerItem)+offset); // set the correct size
	/* end of header */
	if (check_col(pinfo->cinfo, COL_INFO)) {
		if (global_uru_summary_ack) {
			if (global_uru_header_style) {
				col_append_fstr(pinfo->cinfo,
						COL_INFO, "  [%u] %s {%u,%u (%u) %u,%u}",
						packetnum, val_to_str(flags, messagetypes_short, "Unk   "),
						seqnum, fragnum, fragct, lastack, fragack);
			}
			else {
				col_append_fstr(pinfo->cinfo,
						COL_INFO, "  %s seq:%u(%u/%u) prev:%u(%u)",
						val_to_str(flags, messagetypes_short, "Unk   "),
						seqnum, fragnum, fragct, lastack, fragack);
			}
		}
		else {
			col_append_fstr(pinfo->cinfo, COL_INFO, "  %s", val_to_str(flags, messagetypes_short, "Unk   "));
		}
	}

	/* Handle fragments */
	/* code from:
		http://www.wireshark.org/docs/wsdg_html_chunked/ChDissectReassemble.html */
	if (fragct != 0) {
		gboolean save_fragmented;
		tvbuff_t* sub_tvb = NULL;
		fragment_data *frag_msg = NULL;
		
		save_fragmented = pinfo->fragmented;
		pinfo->fragmented = TRUE;
		frag_msg = fragment_add_seq_check(tvb, offset, pinfo,
						seqnum, /* ID for fragments belonging together */
						uru_fragment_table, /* list of message fragments */
						uru_reassembled_table, /* list of reassembled messages */
						fragnum, /* fragment sequence number */
						tvb_length_remaining(tvb, offset), /* fragment length - to the end */
						fragnum != fragct); /* More fragments? */
		
		sub_tvb = process_reassembled_data(tvb, offset, pinfo,
						"Reassembled Message", frag_msg, &uru_frag_items,
						NULL, uru_tree);
	
		if (sub_tvb) { /* last fragment */
			if (check_col(pinfo->cinfo, COL_INFO))
				col_append_fstr(pinfo->cinfo, COL_INFO,
						" (%u fragments reassembled)", fragct);
			tvb = sub_tvb;
		} else { /* some intermediate fragment */
			if (check_col(pinfo->cinfo, COL_INFO))
				col_append_fstr(pinfo->cinfo, COL_INFO,
						" (fragment %u/%u)", fragnum, fragct);
			return;
		}
		pinfo->fragmented = save_fragmented;
	}
	else { /* Not fragmented */
		tvb = tvb_new_subset(tvb, offset, -1, -1);
	}
	offset = 0; // we are at the start of the buffer now

	if (flags == (UNetNegotiation|UNetAckReq) || flags == (UNetNegotiation|UNetAckReq|UNetExt)) {
		/* a negotiation message */
		if (tree) {
			guint32 time[2];
			
			proto_item_append_text(ti, ", Negotiation");
			proto_tree_add_item(uru_tree, hf_uru_bandwidth, tvb, offset, 4, TRUE);
			offset += 4;
			/* see commentary on FT_RELATIVE_TIMESTAMP below for why this code
			is like this */
			time[0] = tvb_get_letohl(tvb, offset);
			time[1] = tvb_get_letohl(tvb, offset+4);
			proto_tree_add_bytes_format(uru_tree, hf_uru_nego_ts, tvb, offset, 8,
						(guint8*)time, "Timestamp: %u.%06u", time[0], time[1]);
			/* for packet filters */
			tf = proto_tree_add_item(uru_tree, hf_uru_nego_sec, tvb,
						offset, 4, TRUE);
			PROTO_ITEM_SET_HIDDEN(tf);
			tf = proto_tree_add_item(uru_tree, hf_uru_nego_usec, tvb,
						offset+4, 4, TRUE);
			PROTO_ITEM_SET_HIDDEN(tf);
			offset += 8;
		}
	}
	else if (flags == UNetAckReply || flags == (UNetAckReply|UNetExt)) {
		/* an ack message */
		if (tree) {
			guint i;
			guint32 sn, snf;
			guint8 frn, frnf;
			guint32 zeros;
			guint32 size;
		
			proto_item_append_text(ti, ", Ack");
			if (!(flags & UNetExt)) {
				zeros = tvb_get_letohs(tvb, offset);
				if (zeros != 0) {
					proto_tree_add_text(uru_tree, tvb, offset, 2,
							"Unknown data in Ack (%04X)", zeros);
					uru_parse_error(uru_tree, tvb, offset, 2, "Unknown data in Ack");
					return;
				}
				offset += 2;
			}
			for (i = 0; i < msglen; i++) {
				frn = tvb_get_guint8(tvb, offset);
				tf = proto_tree_add_item(uru_tree, hf_uru_ack_frn, tvb, offset,
							1, TRUE);
				PROTO_ITEM_SET_HIDDEN(tf);
				sn = tvb_get_letoh24(tvb, offset+1);
				tf = proto_tree_add_item(uru_tree, hf_uru_ack_sn, tvb, offset+1,
							3, TRUE);
				PROTO_ITEM_SET_HIDDEN(tf);
				size = 4;
				if (!(flags & UNetExt)) {
					zeros = tvb_get_letohl(tvb, offset+4);
					if (zeros != 0) {
						proto_tree_add_text(uru_tree, tvb, offset+4, 4,
								"Unknown data in Ack (%08X)", zeros);
						uru_parse_error(uru_tree, tvb, offset+4, 4, "Unknown data in Ack");
						return;
					}
					size += 4;
				}
				frnf = tvb_get_guint8(tvb, offset+size);
				tf = proto_tree_add_item(uru_tree, hf_uru_ack_frnf, tvb, offset+size,
							1, TRUE);
				++size;
				PROTO_ITEM_SET_HIDDEN(tf);
				snf = tvb_get_letoh24(tvb, offset+size);
				tf = proto_tree_add_item(uru_tree, hf_uru_ack_snf, tvb, offset+size,
							3, TRUE);
				size += 3;
				PROTO_ITEM_SET_HIDDEN(tf);
				if (!(flags & UNetExt)) {
					zeros = tvb_get_letohl(tvb, offset+size);
					if (zeros != 0) {
						proto_tree_add_text(uru_tree, tvb, offset+size, 4,
								"Unknown data in Ack (%08X)", zeros);
						uru_parse_error(uru_tree, tvb, offset+size, 4, "Unknown data in Ack");
						return;
					}
					size += 4;
				}
				if (global_uru_header_style) {
					proto_tree_add_none_format(uru_tree, hf_uru_ack, tvb, offset,
								size, "Ack %u,%u %u,%u", sn, frn, snf, frnf);
				}
				else {
					proto_tree_add_none_format(uru_tree, hf_uru_ack, tvb, offset,
								16, "%u(%u) < Ack <= %u(%u)", snf, frnf, sn, frn);
				}
				offset += 16;
			}
		}
	}
	else if (flags == 0x00 || flags == UNetExt || flags == UNetAckReq || flags == (UNetAckReq|UNetExt)) {
		/* a plNetMessage */
		netmsgtype = tvb_get_letohs(tvb, offset);
		if (check_col(pinfo->cinfo, COL_INFO)) {
			col_append_fstr(pinfo->cinfo,
					COL_INFO, "  %s", val_to_str(netmsgtype, plNetMsgs, "Unknown (0x%04x)"));
		}
		if (tree) {
			guint32 unknownFlags;
			guint8* c = ": ";
			
			proto_tree_add_item(uru_tree, hf_uru_cmd, tvb, offset, 2, TRUE);
			proto_item_append_text(ti, ", NetMessage: %s",
						val_to_str(netmsgtype,  plNetMsgs, "Unknown (0x%04x)"));
			offset += 2;
			
			unknownFlags = netmsgflags = tvb_get_letohl(tvb, offset);
			tf = proto_tree_add_item(uru_tree, hf_uru_flags, tvb, offset, 4, TRUE);
			sub_tree = proto_item_add_subtree(tf, ett_netmsgflags);
			proto_tree_add_item(sub_tree, hf_uru_flags_ts, tvb, offset,
						4, TRUE);
			if (netmsgflags & plNetTimestamp) {
				proto_item_append_text(tf, "%sTimestamp", c);
				c = "|";
				unknownFlags &= ~plNetTimestamp; // mask out this flag, it's known
			}
			proto_tree_add_item(sub_tree, hf_uru_flags_receivers, tvb, offset,
						4, TRUE);
			if (netmsgflags & plNetMsgRecvrs) {
				proto_item_append_text(tf, "%sReceivers", c);
				c = "|";
				unknownFlags &= ~plNetMsgRecvrs; // mask out this flag, it's known
			}
			proto_tree_add_item(sub_tree, hf_uru_flags_timeoutok, tvb, offset,
						4, TRUE);
			if (netmsgflags & plNetTimeoutOk) {
				proto_item_append_text(tf, "%sTimeoutOk", c);
				c = "|";
				unknownFlags &= ~plNetTimeoutOk; // mask out this flag, it's known
			}
			proto_tree_add_item(sub_tree, hf_uru_flags_firewalled, tvb, offset,
						4, TRUE);
			if (netmsgflags & plNetFirewalled) {
				proto_item_append_text(tf, "%sFirewalled", c);
				c = "|";
				unknownFlags &= ~plNetFirewalled; // mask out this flag, it's known
			}
			proto_tree_add_item(sub_tree, hf_uru_flags_X, tvb, offset,
						4, TRUE);
			if (netmsgflags & plNetX) {
				proto_item_append_text(tf, "%sX", c);
				c = "|";
				unknownFlags &= ~plNetX; // mask out this flag, it's known
			}
			proto_tree_add_item(sub_tree, hf_uru_flags_newsdl, tvb, offset,
						4, TRUE);
			if (netmsgflags & plNetNewSDL) {
				proto_item_append_text(tf, "%sNewSDL", c);
				c = "|";
				unknownFlags &= ~plNetNewSDL; // mask out this flag, it's known
			}
			proto_tree_add_item(sub_tree, hf_uru_flags_statereq1, tvb, offset,
						4, TRUE);
			if (netmsgflags & plNetStateReq1) {
				proto_item_append_text(tf, "%sStateReq1", c);
				c = "|";
				unknownFlags &= ~plNetStateReq1; // mask out this flag, it's known
			}
			proto_tree_add_item(sub_tree, hf_uru_flags_ki, tvb, offset,
						4, TRUE);
			if (netmsgflags & plNetKi) {
				proto_item_append_text(tf, "%sKi", c);
				c = "|";
				unknownFlags &= ~plNetKi; // mask out this flag, it's known
			}
			proto_tree_add_item(sub_tree, hf_uru_flags_relreg, tvb, offset,
						4, TRUE);
			if (netmsgflags & plNetRelRegions) {
				proto_item_append_text(tf, "%sRelRegions", c);
				c = "|";
				unknownFlags &= ~plNetRelRegions; // mask out this flag, it's known
			}
			proto_tree_add_item(sub_tree, hf_uru_flags_uid, tvb, offset,
						4, TRUE);
			if (netmsgflags & plNetUID) {
				proto_item_append_text(tf, "%sUID", c);
				c = "|";
				unknownFlags &= ~plNetUID; // mask out this flag, it's known
			}
			proto_tree_add_item(sub_tree, hf_uru_flags_directed, tvb, offset,
						4, TRUE);
			if (netmsgflags & plNetDirected) {
				proto_item_append_text(tf, "%sDirected", c);
				c = "|";
				unknownFlags &= ~plNetDirected; // mask out this flag, it's known
			}
			proto_tree_add_item(sub_tree, hf_uru_flags_version, tvb, offset,
						4, TRUE);
			if (netmsgflags & plNetVersion) {
				proto_item_append_text(tf, "%sVersion", c);
				c = "|";
				unknownFlags &= ~plNetVersion; // mask out this flag, it's known
			}
			proto_tree_add_item(sub_tree, hf_uru_flags_system, tvb, offset,
						4, TRUE);
			if (netmsgflags & plNetSystem) {
				proto_item_append_text(tf, "%sSystem", c);
				c = "|";
				unknownFlags &= ~plNetSystem; // mask out this flag, it's known
			}
			proto_tree_add_item(sub_tree, hf_uru_flags_ack, tvb, offset,
						4, TRUE);
			if (netmsgflags & plNetAck) {
				proto_item_append_text(tf, "%sAck", c);
				c = "|";
				unknownFlags &= ~plNetAck; // mask out this flag, it's known
			}
			proto_tree_add_item(sub_tree, hf_uru_flags_sid, tvb, offset,
						4, TRUE);
			if (netmsgflags & plNetSid) {
				/* custom flag (see unet3+) */
				proto_item_append_text(tf, "%sSid", c);
				c = "|";
				unknownFlags &= ~plNetSid; // mask out this flag, it's known
			}
			proto_tree_add_item(sub_tree, hf_uru_flags_p2p, tvb, offset,
						4, TRUE);
			if (netmsgflags & plNetP2P) {
				proto_item_append_text(tf, "%sP2P", c);
				unknownFlags &= ~plNetP2P; // mask out this flag, it's known
			}
			if (unknownFlags)
				uru_parse_error(sub_tree, tvb, offset, 4, "Invalid flags remaining: 0x%08X", unknownFlags);
			offset += 4;
			if (netmsgflags & plNetVersion) {
				guint8 max = tvb_get_guint8(tvb, offset);
				guint8 min = tvb_get_guint8(tvb, offset+1);
				proto_tree_add_none_format(uru_tree, hf_uru_version, tvb, offset,
								2, "Max version: %u, Min version: %u", max, min);
				/* for packet filters */
				tf = proto_tree_add_item(uru_tree, hf_uru_maxversion, tvb,
							offset, 1, TRUE);
				PROTO_ITEM_SET_HIDDEN(tf);
				tf = proto_tree_add_item(uru_tree, hf_uru_minversion, tvb,
							offset+1, 1, TRUE);
				PROTO_ITEM_SET_HIDDEN(tf);
				offset += 2;
			}
			if (netmsgflags & plNetTimestamp) {
				/*proto_tree_add_item(uru_tree, hf_uru_ts, tvb, offset, 8, TRUE);*/
				/* because the above with FT_RELATIVE_TIMESTAMP doesn't work
					(nice of them to say it can be done)... and that expects
					sec.nanoseconds anyway */
				guint32 time[2];
				time[0] = tvb_get_letohl(tvb, offset);
				time[1] = tvb_get_letohl(tvb, offset+4);
				proto_tree_add_bytes_format(uru_tree, hf_uru_ts, tvb, offset, 8,
								(guint8*)time, "Timestamp: %u.%06u", time[0], time[1]);
				/* for packet filters */
				tf = proto_tree_add_item(uru_tree, hf_uru_ts_sec, tvb,
							offset, 4, TRUE);
				PROTO_ITEM_SET_HIDDEN(tf);
				tf = proto_tree_add_item(uru_tree, hf_uru_ts_usec, tvb,
							offset+4, 4, TRUE);
				PROTO_ITEM_SET_HIDDEN(tf);
				offset += 8;
			}
			if (netmsgflags & plNetX) {
				proto_tree_add_item(uru_tree, hf_uru_X, tvb, offset, 4, TRUE);
				offset += 4;
			}
			if (netmsgflags & plNetKi) {
				proto_tree_add_item(uru_tree, hf_uru_KI, tvb, offset, 4, TRUE);
				offset += 4;
			}
			if (netmsgflags & plNetUID) {
				proto_tree_add_item(uru_tree, hf_uru_UID, tvb, offset, 16, FALSE);
				offset += 16;
			}
			/* custom flag (see unet3+) */
			if (netmsgflags & plNetSid) {
				proto_tree_add_item(uru_tree, hf_uru_sid, tvb, offset, 4, TRUE);
				offset += 4;
			}
			
			if (tvb_length_remaining(tvb, offset) > 0) {
				tvbuff_t *ftvb;
				gint bufsize, i;
				guint8 *newbuf;
		
				bufsize = tvb_length_remaining(tvb, offset);
				newbuf = tvb_memdup(tvb, offset, bufsize);
				for (i = 0; i < bufsize; i++) {
					newbuf[i] = ~newbuf[i];
				}
				ftvb = tvb_new_real_data(newbuf, bufsize, bufsize);
				tvb_set_child_real_data_tvbuff(tvb, ftvb);
				add_new_data_source(pinfo, ftvb, "Bit-flipped Message Body");
				tvb_set_free_cb(ftvb, g_free);
				// Dissect plNetMsgs (by diafero)
				tf = proto_tree_add_item(uru_tree, hf_uru_netmsg, tvb, offset, -1, TRUE);
				sub_tree = proto_item_add_subtree(tf, ett_netmsg); // always add the tree - it's invisible when no items are added
				offset = dissectNetMsg(sub_tree, netmsgtype, tvb, offset);
			}
		}
	}
	else { // it's neither ack not nego nor netmsg
		if (tree) {
			proto_item_append_text(ti, ", Unrecognized message type");
			proto_tree_add_text(uru_tree, tvb, offset, -1, "data (%d bytes)",
					tvb_length_remaining(tvb, offset));
		}
	}
	// if there's anything remaining, it's an invalid packet
	if (tree && tvb_length_remaining(tvb, offset) > 0) {
		uru_parse_error(sub_tree, tvb, 0, 0, "%d Bytes left after parsing", tvb_length_remaining(tvb, offset));
	}
}



/* Register the protocol with Wireshark */
void proto_register_uru(void)
{
	module_t *uru_module;

	char tmpstr[64];

	/* Register the protocol name and description */
	if (proto_uru == -1) {
		proto_uru = proto_register_protocol (
			"Uru Protocol", /* name */
			"Uru", /* short name */
			"uru" /* abbrev */
			);
  }

	/* Required function calls to register the header fields and subtrees used */
	proto_register_field_array(proto_uru, hf, array_length(hf));
	proto_register_subtree_array(ett, array_length(ett));


	/* Register preferences module (See Section 2.6 for more on preferences) */
	uru_module = prefs_register_protocol(proto_uru, proto_reg_handoff_uru);

	g_snprintf(tmpstr, sizeof(tmpstr), "%u-%u", URU_PORT_LOW, URU_PORT_HIGH);
	range_convert_str(&global_uru_port_range, tmpstr, 65535);
	prefs_register_range_preference(uru_module, "udp_ports", "Uru Port Range",
				  "Set the port range for Uru messages",
				  &global_uru_port_range, 65535);
	prefs_register_bool_preference(uru_module, "alcugs_format",
				 "Header Format",
				 "Show Alcugs-style header info",
				 &global_uru_header_style);
	prefs_register_bool_preference(uru_module, "alcugs_summaryacks",
				 "Show Ack Info",
				 "Show packet number & ack info in summary",
				 &global_uru_summary_ack);

	/* Register protocol init routine */
	register_init_routine(uru_init_protocol);
}

/* port range logic borrowed from plugins/packet-asn1.c */
static void register_uru_port(guint32 port) {
	dissector_add("udp.port", port, uru_handle);
}

static void unregister_uru_port(guint32 port) {
	dissector_delete("udp.port", port, uru_handle);
}

static void uru_init_protocol(void) {
	fragment_table_init(&uru_fragment_table);
	reassembled_table_init(&uru_reassembled_table);
	if (uru_handle_inited) // add port handlers (settings should be loaded by now)
		proto_reg_handoff_uru();
}

void proto_reg_handoff_uru(void) {
	if (!uru_handle_inited) {
		uru_handle = create_dissector_handle(dissect_uru, proto_uru);
		uru_handle_inited = TRUE;
		return; // do not add port handlers, settings are not yet loaded
	}
	if (uru_port_range != NULL) {
		range_foreach(uru_port_range, unregister_uru_port);
		g_free(uru_port_range);
	}
	uru_port_range = range_copy(global_uru_port_range);
	range_foreach(uru_port_range, register_uru_port);
}
